<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function ( Request $request )
{
    return $request->user();
}
);

/*
 * Account
 */

Route::group([
    'middleware' => 'api',
    'namespace'  => 'Api',
    'prefix'     => 'Api'
], function ()
{
    Route::post("register/checkField", [ 'uses' => 'Api\ApiAccountController@checkField' ])->name('checkField');
}
);

/*
 * Cards
 */

Route::group([
    'middleware' => 'api',
    'namespace'  => 'Api',
    'prefix'     => 'cards'
], function ()
{
    Route::post('default/{id}', 'ApiAccountController@setCardAsDefault')->name('defaultCard');
    Route::delete('remove/{id}', 'ApiAccountController@removeCard')->name('removeCard');
}
);

/*
 * Banks
 */

Route::group([
    'middleware' => 'api',
    'namespace'  => 'Api',
    'prefix'     => 'banks'
], function ()
{
    Route::post('default/{id}', 'ApiAccountController@setBankAsDefault')->name('defaultBank');
    Route::delete('remove/{id}', 'ApiAccountController@removeBank')->name('removeBank');
}
);

/*
 * Video
 */

Route::group([
    'middleware' => 'api',
    'namespace'  => 'Api',
    'prefix'     => 'video'
], function ()
{
    Route::get("listAll", [ 'uses' => 'ApiVideoController@listAll' ])->name('listAll');
    Route::post("searchVideo", [ 'uses' => 'ApiVideoController@searchVideo' ])->name('searchVideo');
    Route::post("searchHosts", [ 'uses' => 'ApiVideoController@searchHosts' ])->name('searchHosts');
}
);

/*
 * Geo
 */

Route::group([
    'middleware' => 'api',
    'namespace'  => 'Api',
    'prefix'     => 'loc'
], function ()
{
    Route::get("countries", [ 'uses' => 'ApiLocationController@getCountries' ])->name('countries');
    Route::get("states", [ 'uses' => 'ApiLocationController@getStates' ])->name('states');
    Route::get("cities", [ 'uses' => 'ApiLocationController@getCities' ])->name('cities');
}
);