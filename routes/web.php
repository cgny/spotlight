<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


/*
 * Main
 */

Route::get('/contact','MainController@Contact')->name('contact');

/*
 * Login / Logout
 */

Route::get('login', 'LoginController@login')->name('login');
Route::get('logout', 'LoginController@logout')->name('logout');
Route::post('processLogin', 'LoginController@do_login')->name('do_login');

/*
 * Account
 */

Route::get('register', 'AccountController@index')->name('register');
Route::post('account/submit', 'AccountController@do_register')->name('do_register');
Route::get('account', 'AccountController@view')->name('account');
Route::get('host/{id}', 'AccountController@host')->name('host');
Route::post('account/update', 'AccountController@do_update')->name('accountUpdate');
Route::get('account/activation', 'AccountController@activate')->name('activation');
Route::get('account/view', 'AccountController@view')->name('home');

/*
 * Cards
 */

Route::get('cards/add', 'AccountController@card')->name('cards');
Route::post('cards/submitCard', 'AccountController@addCard')->name('addCard');
Route::get('cards/manage', 'AccountController@manageCards')->name('manageCards');


/*
 * Banks
 */

Route::get('banks/add', 'AccountController@bank')->name('banks');
Route::post('banks/submitBank', 'AccountController@addBank')->name('addBank');
Route::get('banks/manage', 'AccountController@manageBanks')->name('manageBank');
Route::get('banks/view/{id}', 'AccountController@viewBank')->name('viewBank');


/*
 * Videos
 */

Route::get('videos', 'VideoController@video')->name('video');
Route::get('hosts', 'VideoController@hosts')->name('hosts');
Route::get('video/token', 'VideoController@genToken')->name('token');
Route::get('watch', 'VideoController@index')->name('index');
Route::get('video/start', 'VideoController@st_log')->name('start');
Route::get('calendar', 'VideoController@calendar')->name('calendar');
Route::get('video', 'VideoController@listCreated')->name('listCreated');
Route::get('video/purchased', 'VideoController@listPurchased')->name('listPurchased');
Route::get('video/create', 'VideoController@create')->name('create');
Route::post('video/upload', 'VideoController@do_create')->name('videoUpload');
Route::get('video/edit', 'VideoController@edit')->name('videoEdit');
Route::post('video/update', 'VideoController@do_edit')->name('videoUpdate');