<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers as Controller;

use \App\Models;

class ApiVideoController extends Controller\Controller
{
    private $video, $error;

    function __construct()
    {
        $this->video = new Models\Video();
    }

    function searchVideo( Request $request )
    {
        $search  = $request->input('search');
        $page    = (int) $request->input('page');
        $filters = $request->input('filters');
        $results = $this->video->searchVideo($search, $filters, $page);
        return response()->json(['videos' => $results, 'count' => count($results)]);
    }

    function searchHosts( Request $request )
    {
        $search  = $request->input('search');
        $page    = (int) $request->input('page');
        $filters = $request->input('filters');
        $results = $this->video->searchHosts($search, $filters, $page);
        return response()->json(['hosts' => $results, 'count' => count($results)]);
    }

    function listAll()
    {
        $login   = new Models\Login();
        $account = $login->isLogged();

        $videos = $this->video->getAllStreams(14, false, false, true, true);
        return response()->json(['video' => $videos, 'count' => count($videos)]);
    }

}