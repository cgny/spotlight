<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers as Controller;

use App\Models;

class ApiAccountController extends Controller\Controller
{
    //
    private $account, $data, $login;

    function __construct()
    {

    }

    function update()
    {

    }

    function updateStripeCard()
    {

    }

    function checkField( Request $request )
    {
        $validatedData = $request->validate([
                'field' => [ 'required', 'max:255' ],
                'value' => [ 'required', 'max:255' ]
            ]
        );

        $this->data    = new Models\Data();
        $this->account = new Models\Account();
        $field         = $this->data->cleanData($request->input('field'));
        $value         = $this->data->cleanData($request->input('value'));

        if ($field == 'email')
        {

            return response()->json((object) [ 'count' => $this->account->checkEmail($value) ]);
        }
        elseif ($field == 'username')
        {
            return response()->json((object) [ 'count' => $this->account->checkUsername($value) ]);
        }
    }

    function setCardAsDefault( $id )
    {
        $this->account = new Models\Account();
        $this->login   = new Models\Login();

        $result = $this->account->setAsDefaultCard($id);
        return response()->json([ 'cards' => $result ]);
    }

    function removeCard( $id )
    {
        $this->account = new Models\Account();
        $this->login   = new Models\Login();

        $result = $this->account->removeCard($id);
        return response()->json([ 'cards' => $result ]);
    }

    function setBankAsDefault( $id )
    {
        $this->account = new Models\Account();
        $this->login   = new Models\Login();

        $result = $this->account->setAsDefaultBank($id);
        return response()->json([ 'banks' => $result ]);
    }

    function removeBank( $id )
    {
        $this->account = new Models\Account();
        $this->login   = new Models\Login();

        $result = $this->account->removeBank($id);
        return response()->json([ 'banks' => $result ]);
    }

}