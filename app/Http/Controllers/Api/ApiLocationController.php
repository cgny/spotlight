<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers as Controller;

use App\Models;

class ApiLocationController extends Controller\Controller
{
    private $data;

    function __construct()
    {
        $this->data = new Models\Data();
    }

    function getCountries( Request $request )
    {
        $countries = $this->data->getCountries();
        return response()->json([ 'countries' => $countries ]);
    }

    function getStates( Request $request )
    {
        $country   = (int) $request->input('country_id');
        $states = $this->data->getStates($country);
        return response()->json([ 'states' => $states ]);
    }

    function getCities( Request $request )
    {
        $country   = (int) $request->input('country_id');
        $state     = (int) $request->input('state_id');
        $cities = $this->data->getCities($country, $state);
        return response()->json([ 'cities' => $cities ]);
    }
}