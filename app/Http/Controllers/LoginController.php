<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models;


class LoginController extends Controller
{
    private $login, $account;
    function __construct()
    {
    }

    function index(Request $request)
    {
        return false;
    }

    function login(Request $request)
    {
        return view('login.login');
    }

    function do_login(Request $request)
    {
        $this->login = new Models\Login();

        $username = $request->input('username');
        $password = $request->input('password');

        if( $this->login->login($username,$password) )
        {
            return redirect('account');
        }
        else
        {
            return redirect('login');
        }
    }

    function forgotPassword()
    {

    }

    function do_forgotPassword(Request $request)
    {

    }

    function logout(Request $request)
    {
        $this->login = new Models\Login();
        $this->login->logout($request);
        return redirect('login');

    }


}