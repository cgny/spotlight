<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models;


class AuthController extends Controller
{
    public $links, $token, $user, $user_id;
    private  $account, $url, $cart;

    function __construct()
    {
        parent::__construct();
        $this->account = new Models\Account();
        $this->cart    = new Models\Cart();
        $this->url     = new Models\Url();
    }

    public function index( Request $request )
    {
        $this->middleware('auth');
        $auth_code = $request->input('code');
        if (empty($auth_code))
        {
            return redirect()->away(config('app.url'));
        }
        else
        {
            if (isset($data->access_token))
            {
                $_SESSION['user'] = $this->account->authenticateAccount($auth_code);
                $_SESSION['user_id']    = $_SESSION['user']->sp_a_id;
                $this->cart->updateCartAccountId();
            }
            return redirect('/');
        }
    }
}
