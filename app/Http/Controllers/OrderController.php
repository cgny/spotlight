<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models;


class OrderController extends Controller
{
    private $login, $account, $order;

    function __construct()
    {

    }

    function view(Request $request)
    {
        $cart_id = $request->input('cart_id');

        $this->order = new Models\Orders();
        $order = $this->order->setOrder( $cart_id );
        return view('cart.order', ['order' => $order]);

    }

    function addToOrder()
    {

    }

    function removeFromOrde()
    {

    }

    function updateOrder()
    {

    }
}