<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models;

class AccountController extends Controller
{
    private $login, $account, $data, $stripe, $video;

    function __construct()
    {

    }

    function index()
    {
        $login = new Models\Login();
        if ($account = $login->isLogged())
        {
            return redirect('account/view');
        }
        else
        {
            return redirect('login');
        }
    }

    function register()
    {
        $this->data  = new Models\Data();
        $this->video = new Models\Video();
        $countries   = $this->data->getCountries();
        $states      = $this->data->getStates();
        $cities      = $this->data->getCities();
        $time_zones  = $this->data->getTimezones();
        $currencies  = $this->data->getCurrencies();
        $categories  = $this->video->getCategories();
        return view('account.register',
            [
                'time_zones' => $time_zones,
                'currencies' => $currencies,
                'categories' => $categories,
                'countries'  => $countries,
                'states'     => $states,
                'cities'     => $cities,
            ]
        );
    }

    function do_register( Request $request )
    {
        $first_name  = $request->input('first_name');
        $last_name   = $request->input('last_name');
        $username    = $request->input('username');
        $phone       = $request->input('phone');
        $email       = $request->input('email');
        $type        = $request->input('type');
        $time_zome   = $request->input('time_zome');
        $currency    = $request->input('currency');
        $country     = $request->input('country');
        $description = $request->input('description');

        $img_name = false;
        if ($request->hasFile('profile_img'))
        {
            $extension  = $request->file('profile_img')->extension();
            $img_name   = md5(rand(100, 500) . time()) . "." . $extension;
            $image_file = $request->file('profile_img')->storeAs(
                'public', $img_name
            );
        }

        $user = [
            'first_name'  => $first_name,
            'last_name'   => $last_name,
            'username'    => $username,
            'phone'       => $phone,
            'email'       => $email,
            'type'        => $type,
            'description' => $description,
            'time_zome'   => $time_zome,
            'currency'    => $currency,
            'country'     => $country,
            'code'        => substr(md5(rand(1, 9999999)), 0, 15),
            'photo'       => $img_name
        ];

        $account = new Models\Account();
        $success = $account->saveAccount($user);

        if ($success)
        {
            $user['full_name'] = $user['first_name'] . ' ' . $user['last_name'];
            $user['url']       = url('/account/activation?code=' . $user['code']);
            $email             = new Models\Email();
            $result            = $email->sendEmailNotification(1, $user);
            print_r([ $success, $user, $result ]);
            exit;
            return redirect('login');
        }
        return redirect('register')->withInput();
    }

    function activate( Request $request )
    {
        $code    = $request->input('code');
        $account = new Models\Account();
        if ($account->activateAccount($code))
        {
            return redirect('account');
        }
        else
        {
            return false; //TODO: bad code view
        }
    }

    function host( $id )
    {
        $this->account = new Models\Account();
        $this->video   = new Models\Video();
        $account       = $this->account->getAccountByHashId($id);

        $created_videos = $this->video->getAllStreams($account->sp_a_id);

        return view('account.host',
            [
                'account'        => $account,
                'created_videos' => $created_videos
            ]
        );
    }

    function view()
    {
        $this->login   = new Models\Login();
        $this->data    = new Models\Data();
        $this->video   = new Models\Video();
        $this->account = new Models\Account();
        $this->account->getUser();
        if ($account = $this->login->isLogged())
        {
            $time_zones         = $this->data->getTimezones();
            $currencies         = $this->data->getCurrencies();
            $countries          = $this->data->getCountries();
            $states             = $this->data->getStates($account->sp_a_country);
            $cities             = $this->data->getCities($account->sp_a_country, $account->sp_a_state);
            $mcc                = $this->data->readCSV('mcc.csv');
            $categories         = $this->video->getCategories();
            $account_categories = json_decode($account->sp_a_categories);
            return view('account.account',
                [
                    'account'            => $account,
                    'time_zones'         => $time_zones,
                    'currencies'         => $currencies,
                    'categories'         => $categories,
                    'states'             => $states,
                    'cities'             => $cities,
                    'mccs'               => $mcc,
                    'countries'          => $countries,
                    'account_categories' => $account_categories
                ]
            );
        }
        else
        {
            return redirect('login');
        }
    }

    function update()
    {

    }

    function card()
    {
        $this->login  = new Models\Login();
        $this->data   = new Models\Data();
        $this->stripe = new Models\Stripe();
        if ($account = $this->login->isLogged())
        {
            $currencies = $this->data->getCurrencies();
            $countries  = $this->data->getCountries();
            $states     = $this->data->getStates($account->sp_a_country);
            $cities     = $this->data->getCities($account->sp_a_country, $account->sp_a_state);
            $intent     = $this->stripe->createIntent($account->sp_a_id);
            return view('account.addCard',
                [
                    'account'    => $account,
                    'currencies' => $currencies,
                    'cities'     => $cities,
                    'states'     => $states,
                    'countries'  => $countries,
                    'intent'     => $intent
                ]
            );
        }
        else
        {
            return redirect('login');
        }
    }

    function manageCards()
    {
        $login         = new Models\Login();
        $this->account = new Models\Account();
        if ($account = $login->isLogged())
        {
            $cards = $this->account->getCards($account->sp_a_id);
            return view('account.manageCards',
                [
                    'account' => $account,
                    'cards'   => $cards,
                ]
            );
        }
        else
        {
            return redirect('login');
        }
    }

    /*
     *
     * add and updates card
     *
     */

    //TODO: check if card info already exists

    function addCard( Request $request )
    {
        print_r($request->user());

        $this->account = new Models\Account();
        $this->login   = new Models\Login();
        $this->data    = new Models\Data();

        if (empty($account = $this->login->isLogged()))
        {
            exit;
        }

        $data = [
            'saved_name'  => $request->input('saved_name'),
            'si_id' => $request->input('si_id'),
            'pm_id' => $request->input('pm_id'),
            's_default' => $request->input('s_default')
        ];
        $data = $this->data->cleanData($data);

        $result = $this->account->addCustomerCard($account, $data);
        return response()->json($result);
    }

    function bank()
    {
        $login         = new Models\Login();
        $this->data    = new Models\Data();
        $this->account = new Models\Account();
        if ($account = $login->isLogged())
        {
            $currencies = $this->data->getCurrencies();
            $countries  = $this->data->getCountries();
            $states     = $this->data->getStates($account->sp_a_country);
            $cities     = $this->data->getCities($account->sp_a_country, $account->sp_a_state);
            return view('account.addBank',
                [
                    'account'    => $account,
                    'currencies' => $currencies,
                    'countries'  => $countries,
                    'cities'     => $cities,
                    'states'     => $states,
                ]
            );
        }
        else
        {
            return redirect('login');
        }
    }

    function manageBanks()
    {
        $login         = new Models\Login();
        $this->data    = new Models\Data();
        $this->account = new Models\Account();
        if ($account = $login->isLogged())
        {
            $currencies = $this->data->getCurrencies();
            $countries  = $this->data->getCountries();
            $banks      = $this->account->getBankAccounts($account->sp_a_id);
            return view('account.manageBanks',
                [
                    'account'    => $account,
                    'currencies' => $currencies,
                    'countries'  => $countries,
                    'banks'      => $banks
                ]
            );
        }
        else
        {
            return redirect('login');
        }
    }

    //TODO: check if account info already exists

    function addBank( Request $request )
    {
        $this->account = new Models\Account();
        $this->login   = new Models\Login();
        $this->data    = new Models\Data();

        if (empty($account = $this->login->isLogged()))
        {
            exit;
        }

        $name        = $request->input('saved_name');
        $bank        = $request->input('a_bank');
        $account_n   = $request->input('a_number');
        $routing     = $request->input('r_number');
        $country     = $request->input('country');
        $currency    = $request->input('currency');
        $stripeToken = $request->input('stripeToken');

        $data = [
            'sp_a_id'                => $account->sp_a_id,
            'sp_b_name'              => $name,
            'sp_b_bank'              => $bank,
            'sp_b_account_number_4'  => substr($account_n, -4),
            'sp_b_account_routing_4' => substr($routing, -4),
            'sp_b_country'           => $country,
            'sp_b_currency'          => $currency,
        ];

        $response = $this->account->addAccountBank($account, $stripeToken, $data);
        return response()->json($response);
    }

    function do_update( Request $request )
    {
        $this->account = new Models\Account();
        $this->login   = new Models\Login();
        $this->data    = new Models\Data();

        if (empty($account = $this->login->isLogged()))
        {
            $data['errors']['message'] = "Not loged in";
            return response()->json($data);
            exit;
        }

        $account_id = $account->sp_a_id;

        $first_name    = $request->input('first_name');
        $last_name     = $request->input('last_name');
        $email         = $request->input('email');
        $phone         = $request->input('phone');
        $currency      = $request->input('currency');
        $country       = $request->input('country');
        $description   = $request->input('description');
        $categories    = json_encode($request->input('categories'));
        $business_name = $request->input('business_name');
        $business_url  = $request->input('business_url');
        $address_1     = $request->input('address_1');
        $address_2     = $request->input('address_2');
        $city          = $request->input('city');
        $state         = $request->input('state');
        $postal_code   = $request->input('postal');
        $ein           = $request->input('ein');
        $dob_m         = $request->input('dob_m');
        $dob_d         = $request->input('dob_d');
        $dob_y         = $request->input('dob_y');
        $mcc           = $request->input('mcc');

        $img_name = $account->sp_a_photo;
        if ($request->hasFile('profile_img'))
        {
            $extension  = $request->file('profile_img')->extension();
            $img_name   = md5($account_id . time()) . "." . $extension;
            $image_file = $request->file('profile_img')->storeAs(
                'public', $img_name
            );
        }

        $update = array(
            'sp_a_first_name'    => $first_name,
            'sp_a_last_name'     => $last_name,
            'sp_a_email'         => $email,
            'sp_a_phone'         => $phone,
            'sp_a_currency'      => $currency,
            'sp_a_country'       => $country,
            'sp_a_description'   => $description,
            'sp_a_categories'    => $categories ?? [],
            'sp_a_business_name' => $business_name,
            'sp_a_business_url'  => $business_url,
            'sp_a_address_1'     => $address_1,
            'sp_a_address_2'     => $address_2,
            'sp_a_city'          => $city,
            'sp_a_state'         => $state,
            'sp_a_postal_code'   => $postal_code,
            'sp_a_tax_id'        => $ein,
            'sp_a_dob_m'         => (int) $dob_m,
            'sp_a_dob_d'         => (int) $dob_d,
            'sp_a_dob_y'         => (int) $dob_y,
            'sp_a_photo'         => $img_name,
            'sp_a_industry'      => $mcc,
        );

        $update  = $this->data->cleanData($update);
        $success = $this->account->updateAccount($account_id, $update, true);
        $user    = $this->account->getUser();

        return redirect('account');
    }

    function getMySales()
    {
        $this->account = new Models\Account();
        $sales         = $this->account->getMySales();
    }

    function authorizeStripe( Request $request )
    {
        $this->data   = new Models\Data();
        $this->stripe = new Models\Stripe();

        $code = $this->data->cleanData($request->input('code'));
        $auth = $this->data->cleanData($this->stripe->authorizeAccount($code));

        //TODO
    }

    function getCountries()
    {
        $this->account = new Models\Account();
        $this->login = new Models\Login();
        if (!empty($account = $this->login->isLogged()))
        {
            //$data = $this->data->getCountries();
            //echo json_encode( $data );
        }
    }

    function getCurrencies()
    {
        $this->account = new Models\Account();
        $this->login = new Models\Login();
        if (!empty($account = $this->login->isLogged()))
        {
            //$data = $this->data->getCurrencies();
            //echo json_encode( $data );
        }
    }

    function viewBank( $id )
    {
        $this->data    = new Models\Data();
        $this->account = $account = new Models\Account();
        $this->login = new Models\Login();
        if (!empty($account = $this->login->isLogged()))
        {
            $id   = $this->data->cleanData($id);
            $bank = $this->account->getBankByStripeId($id);

            $currencies = $this->data->getCurrencies();
            $countries  = $this->data->getCountries();
            $states     = $this->data->getStates($account->sp_a_country);
            $cities     = $this->data->getCities($account->sp_a_country, $account->sp_a_state);
            return view('account.editBank',
                [
                    'account'    => $account,
                    'currencies' => $currencies,
                    'countries'  => $countries,
                    'cities'     => $cities,
                    'states'     => $states,
                    'bank'       => $bank
                ]
            );
        }
    }


}