<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\App;

use App\Models;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;
use Twilio\Rest\Client;

class VideoController extends Controller
{
    protected $video, $login, $data, $account, $twilio, $sid, $key, $secret;

    function __construct()
    {
        $this->login   = new Models\Login();
        $this->account = $this->login->isLogged();
        if ($this->login->isLogged())
        {
            return redirect('login');
        }

        $this->sid    = "AC35548be8993c26f2682228f8437b6175";
        $this->key    = "SKc2c4b03d5307b1113b8dd0c0641bbc9d";
        $this->secret = "9B1uec6q4gGalxd827h4e20LaPG2ZF6D";

    }

    function video( Request $request )
    {
        $this->video = new Models\Video();
        $account     = $this->login->isLogged();
        $page        = (int) $request->input('page');

        $categories = $this->video->getCategories();
        $videos     = $this->video->getAllStreams($account->sp_a_id, false, false, true, true, 20, $page);
        return view('videos.search', [ 'account' => $account, 'videos' => $videos, 'categories' => $categories ]);
    }

    function hosts( Request $request )
    {
        $this->video = new Models\Video();
        $account     = $this->login->isLogged();
        $page        = (int) $request->input('page');

        $categories = $this->video->getCategories();
        $hosts      = $this->video->getAllHosts(20, $page);
        return view('videos.hosts', [ 'account' => $account, 'hosts' => $hosts, 'categories' => $categories ]);
    }

    function genToken( Request $request )
    {
        $this->video = new Models\Video();
        $account     = $this->login->isLogged();
        $token       = new AccessToken($this->sid, $this->key, $this->secret, 3600, $account->sp_a_first_name);

        $video_data = $this->video->grantAccountStream($request->input('v_id'), $account->sp_a_id);
        $videoGrant = new VideoGrant();
        $videoGrant->setRoom($video_data->sp_s_stream_id);

        // Add grant to token
        $token->addGrant($videoGrant);
        // render token to string
        echo json_encode([ 'token' => $token->toJWT() ]);
    }

    //TODO: check if video file or live stream

    function index( Request $request )
    {
        $this->account = new Models\Account();
        $this->video   = new Models\Video();
        $this->data    = new Models\Data();


        $account = $this->login->isLogged();
        $v_id    = $this->data->cleanData($request->input('v_id'));

        $video_data = $this->video->grantAccountStream($v_id, $account->sp_a_id);

        if ($video_data == false)
        {
            return redirect('video');
        }
        else
        {
            $data_ = array(
                'sp_vc_account_id' => $account->sp_a_id,
                'sp_vc_stream_id'  => $v_id,
                'sp_vc_start'      => date("Y-m-d H:i:s"),
                'sp_vc_end'        => date("Y-m-d 00:00:00")
            );
            $this->video->save_video_log($data_);

            return view('videos.video', [ 'account' => $account, 'video_data' => $video_data ]);
        }

    }

    function calendar()
    {
        $this->video   = new Models\Video();
        $this->login   = new Models\Login();
        $this->account = $this->login->isLogged();

        $purchased_videos = $this->video->getPaidStreamsByAccount($this->account->sp_a_id);
        $created_videos   = $this->video->getAllStreams($this->account->sp_a_id);

        return view('videos.calendar',
            [
                'account'          => $this->account,
                'purchased_videos' => $purchased_videos,
                'created_videos'   => $created_videos
            ]
        );
    }

    function create()
    {
        $this->video   = new Models\Video();
        $this->login   = new Models\Login();
        $this->account = $this->login->isLogged();
        if (!$this->login->isLogged())
        {
            return redirect('login');
        }
        $categories = $this->video->getCategories();
        return view('videos.create', [ 'account' => $this->account, 'categories' => $categories ]);
    }

    //TODO: set max size and max upload amount

    function do_create( Request $request )
    {
        $this->account = $this->login->isLogged();

        $this->video = new Models\Video();

        $v_id = md5($this->account->sp_a_id . $request->input('name') . time());
        $v_id = substr($v_id, 0, 20);

        $img_name = false;
        if ($request->hasFile('video_image'))
        {
            $extension  = $request->file('video_image')->extension();
            $img_name   = md5($this->account->sp_a_id . time()) . "." . $extension;
            $image_file = $request->file('video_image')->storeAs(
                'public', $img_name
            );
        }

        $type       = $request->input('type');
        $video_name = false;
        if ($request->hasFile('video_file'))
        {
            if ($type == 2)
            {
                $extension  = $request->file('video_file')->extension();
                $videos     = $this->video->getAllStreams($this->account->sp_a_id);
                $video_name = $this->account->sp_a_id . '_' . (count($videos) + 1) . "." . $extension;
                $video_file = $request->file('video_file')->storeAs(
                    'public', $video_name
                );
            }
        }

        $title = $request->input('title');
        if ($type == 1)
        {
            $sid          = "AC35548be8993c26f2682228f8437b6175";
            $token        = "e46db037d08332cd4cb0c4a8832483cf";
            $key          = "";
            $this->twilio = new Client($sid, $token);
            $v_id         = $this->twilio->video->v1->rooms
                ->create([
                        "recordParticipantsOnConnect" => True,
                        "statusCallback"              => config('app.url'),
                        "type"                        => "group",
                        "uniqueName"                  => $title
                    ]
                )->sid;
        }

        $data = array(
            'sp_s_stream_id'      => $v_id,
            'sp_s_host_id'        => $this->account->sp_a_id,
            'sp_s_title'          => $title,
            'sp_s_image_file'     => $img_name,
            'sp_s_video_file'     => $video_name,
            'sp_s_info'           => $request->input('info'),
            'sp_s_categories'     => json_encode($request->input('categories')),
            'sp_s_active'         => $request->input('active'),
            'sp_s_type'           => $type,
            'sp_s_rate'           => $request->input('rate'),
            'sp_s_access_length'  => $request->input('join_start'),
            'sp_s_stream_start'   => strtotime($request->input('start_date')),
            'sp_s_stream_end'     => strtotime($request->input('end_date')),
            'sp_s_stream_expire'  => strtotime($request->input('expire')),
            'sp_s_recurring'      => $request->input('recurring'),
            'sp_s_recurring_days' => json_encode($request->input('recurring_days')),
            'sp_s_updated'        => date("Y-m-d H:i:s"),
        );

        $r = $this->video->saveStream($data);
        if ($r === true)
        {
            return redirect('video');
        }
    }

    function edit( Request $request )
    {
        $this->video   = new Models\Video();
        $this->login   = new Models\Login();
        $this->account = $this->login->isLogged();
        if (!$this->login->isLogged())
        {
            return redirect('login');
        }

        $this->video = new Models\Video();

        $v_id  = (string) $request->input('v_id');
        $video = $this->video->getStreamById($v_id, $this->account->sp_a_id);
        if ($video)
        {
            $categories = $this->video->getCategories();

            $cats = $video->sp_s_categories;
            if (empty($video->sp_s_categories))
            {
                $cats = json_encode([]);
            }
            $video_categories = json_decode($cats);

            $days = $video->sp_s_recurring_days;
            if (empty($video->sp_s_recurring_days))
            {
                $days = json_encode([]);
            }
            $video_days = json_decode($days);
            return view('videos.edit',
                [
                    'account'          => $this->account,
                    'video'            => $video,
                    'categories'       => $categories,
                    'video_categories' => $video_categories,
                    'video_days'       => $video_days
                ]
            );
        }
        return redirect('video');

    }

    function do_edit( Request $request )
    {
        $this->login   = new Models\Login();
        $this->account = $this->login->isLogged();

        $this->video = new Models\Video();
        $v_id        = (string) $request->input('v_id');

        $video = $this->video->getStreamById($v_id, $this->account->sp_a_id);

        $img_name = $video->sp_s_image_file;
        if ($request->hasFile('video_image'))
        {
            Storage::delete([ $img_name ]);
            $extension  = $request->file('video_image')->extension();
            $img_name   = md5($this->account->sp_a_id . time()) . "." . $extension;
            $image_file = $request->file('video_image')->storeAs(
                'public', $img_name
            );
        }

        $video_name = $video->sp_s_video_file;
        $type       = $request->input('type');
        if ($type == 2)
        {
            if ($request->hasFile('video_file'))
            {
                Storage::delete([ $video_name ]);
                $extension  = $request->file('video_file')->extension();
                $videos     = $this->video->getAllStreams($this->account->sp_a_id);
                $video_name = $this->account->sp_a_id . '_' . (count($videos) + 1) . "." . $extension;
                $video_file = $request->file('video_file')->storeAs(
                    'public', $video_name
                );
            }
        }

        $data = array(
            'sp_s_title'          => $request->input('title'),
            'sp_s_image_file'     => $img_name,
            'sp_s_video_file'     => $video_name,
            'sp_s_info'           => $request->input('info'),
            'sp_s_categories'     => json_encode($request->input('categories')),
            'sp_s_active'         => $request->input('active'),
            'sp_s_type'           => $type,
            'sp_s_rate'           => $request->input('rate'),
            'sp_s_access_length'  => $request->input('join_start'),
            'sp_s_stream_start'   => strtotime($request->input('start_date')),
            'sp_s_stream_end'     => strtotime($request->input('end_date')),
            'sp_s_stream_expire'  => strtotime($request->input('expire')),
            'sp_s_recurring'      => $request->input('recurring'),
            'sp_s_recurring_days' => json_encode($request->input('recurring_days')),
            'sp_s_updated'        => date("Y-m-d H:i:s"),
        );

        $r = $this->video->updateStream($v_id, $data);
        if ($r == true)
        {
            $room_data = $this->video->getStreamById($v_id);
            if ($request->input('send_invites') == 1)
            {
                $subscribers = $this->video->getAccountsByVideoId($v_id);

                //TODO: send email

            }
            return redirect('video');
        }

    }

    function download( Request $request )
    {
        $this->login   = new Models\Login();
        $this->account = $this->login->isLogged();

        $this->video = new Models\Video();
        $v_id        = (string) $request->input('v_id');
        $video       = $this->video->getStreamById($v_id, $this->account->sp_a_id);
        //return Storage::download('file.jpg');
    }

    function listCreated()
    {
        $this->login = new Models\Login();
        $account     = $this->login->isLogged();

        $this->video = new Models\Video();
        $videos      = $this->video->getAllStreams("", $account->sp_a_id);
        return view('videos.list', [ 'account' => $account, 'videos' => $videos ]);
    }

    function listPurchased()
    {
        $this->login = new Models\Login();
        $account     = $this->login->isLogged();

        $this->video = new Models\Video();
        $videos      = $this->video->getAllStreams($account->sp_a_id);
        return view('videos.purchased', [ 'account' => $account, 'videos' => $videos ]);
    }

    function inviteUser( Request $request )
    {
        if (!isset($_SESSION['logged']))
        {
            exit();
        }

        $user_invite = $this->input->get('u_invite');
        $room_id     = $this->input->get('r_id');
        $room_data   = $this->video->get_room($_SESSION['account_id'], $room_id);
        if ($room_data->v_created_by == $_SESSION['user_id'])
        {

            $users = explode(",", $room_data->v_invitees);
            $data  = array( 'v_invitees' => serialize(array_merge(array( $user_invite ), $users)) );
            $r     = $this->video->update_room($_SESSION['account_id'], $room_id, $data);
            if ($r === true)
            {
                echo "User added";
            }
            else
            {
                echo "User not added";
            }
        }
        else
        {
            echo "Only the creator of the room can invite users";
        }

    }

    function st_log( Request $request )
    {
        $this->video = new Models\Video();

        $account = $this->login->isLogged();

        $vc_id      = $request->input('v_id');
        $account_id = $account->sp_a_id;
        $email      = $account->sp_a_email;
        $data       = array( 'sp_vc_start' => date("Y-m-d H:i:s") );

        $result = $this->video->update_video_log($vc_id, $account_id, $email, $data, 1);
        echo json_encode([ 'success' => 1, 'vc_id' => $vc_id, 'error' => ($result) ]);
    }

    function en_log( Request $request )
    {
        $this->video = new Models\Video();

        $vc_id      = $_SESSION['vc_id'];
        $account_id = $_SESSION['v_account_id'];
        $email      = $_SESSION['v_email'];
        $data       = array( 'vc_end' => date("Y-m-d H:i:s") );
        $result     = $this->video->update_video_log($vc_id, $account_id, $email, $data, '', 1);
        if ($result == true)
        {
            $array = array( 'success' => 1, 'vc_id' => $vc_id, 'error' => "false" );
            echo json_encode($array);
        }

    }


}