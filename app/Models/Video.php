<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Video extends Model
{
    private $account, $data, $error, $user_id, $stripe;
    protected $table = "sp_streams";

    function __construct( array $attributes = [] )
    {
        parent::__construct($attributes);
        $this->user_id = session('user_id');
    }

    function save_video_log( $data )
    {
        return DB::table('sp_video_conference_log')->insert($data);
    }

    function update_video_log( $vc_id, $account_id, $data, $start = false, $end = false )
    {
        $video = DB::table('sp_video_conference_log')
            ->where('sp_vc_id', $vc_id)
            ->where('sp_vc_account_id', $account_id)
            ->first();

        if (empty($video))
        {
            return false;
        }
        else
        {
            if ($start)
            {
                if ($video->sp_vc_start == "0000-00-00 00:00:00")
                {
                    //unstarted session

                    DB::table('sp_video_conference_log')
                        ->where('sp_vc_id', $vc_id)
                        ->update($data);
                }
                elseif ($video->sp_vc_start != "0000-00-00 00:00:00")
                {
                    if ($video->sp_vc_end != "0000-00-00 00:00:00")
                    {
                        //closed session found
                        $data_                = unserialize($_SESSION['video_log']);
                        $data_['sp_vc_start'] = date("Y-m-d H:i:s");
                        $_SESSION['sp_vc_id']    = $this->save_video_log($data_);
                    }
                    else
                    {
                        //open session found, close open, open new one
                        $data['sp_vc_end'] = date("Y-m-d H:i:s");
                        DB::table('sp_video_conference_log')
                            ->where('sp_vc_id', $vc_id)
                            ->update($data);

                        $data_['sp_vc_end'] = date("Y-m-d H:i:s");
                        $_SESSION['sp_vc_id']    = $this->save_video_log($data_);
                    }
                }
            }
            if ($end)
            {
                if ($video->sp_vc_end != "0000-00-00 00:00:00")
                {
                    //session closed
                    return true;
                }
                elseif ($video->sp_vc_end == "0000-00-00 00:00:00")
                {
                    //open session need closed
                    $data['sp_vc_end'] = date("Y-m-d H:i:s");
                    DB::table('sp_video_conference_log')
                        ->where('sp_vc_id', $vc_id)
                        ->update($data);
                }
            }

        }

    }

    function checkAccountStreamAccess( $v_id, $account_id )
    {
        $sql = sprintf("SELECT * FROM sp_payments p
   JOIN sp_streams s ON (s.sp_s_stream_id = p.sp_p_stream_id)
   WHERE sp_s_stream_id = %d
   AND sp_p_success = 1
   AND SEC_TO_TIME(UNIX_TIMESTAMP(sp_p_stream_date)+sp_s_access_length)) > NOW()
   AND sp_s_stream_date <= NOW()
   AND sp_p_account_id = %s",$v_id,$account_id);
        return DB::select(DB::raw($sql));
    }

    function grantAccountStream( $v_id, $account_id )
    {
        return DB::table('sp_streams')
            ->join('sp_payments','sp_payments.sp_p_stream_id', '=', 'sp_streams.sp_s_stream_id')
            ->where('sp_p_success',1)
            ->where('sp_s_stream_id',$v_id)
            ->where('sp_p_account_id',$account_id)
            ->first();
    }

    function getPaidStreamsByAccount( $account_id )
    {
        return DB::table('sp_streams')
            ->join('sp_payments','sp_payments.sp_p_stream_id', '=', 'sp_streams.sp_s_id')
            ->where('sp_p_success',1)
            ->where('sp_p_account_id',$account_id)
            ->get();
    }

    //TODO: find purpose

    function getPaidStreamsByHost( $account_id )
    {
        return DB::table('sp_streams')
            ->join('sp_payments','sp_payments.sp_p_stream_id', '=', 'sp_streams.sp_s_id')
            ->join('sp_accounts','sp_accounts.sp_a_id', '=', 'sp_payments.sp_p_account_id')
            ->where('sp_s_host_id',$account_id)
            ->get();
    }

    function getStreamById( $stream_id, $account_id = false )
    {
        $qry = DB::table('sp_streams');
        $qry->where('sp_s_stream_id',$stream_id);
        if($account_id)
        {
            $qry->where('sp_s_host_id',$account_id);
        }
        return $qry->first();
    }

    function getAccountsByVideoId( $v_id )
    {
        return DB::table('sp_streams')
            ->join('sp_payments','sp_payments.sp_p_stream_id', '=', 'sp_streams.sp_s_id')
            ->join('sp_accounts','sp_accounts.sp_a_id', '=', 'sp_payments.sp_p_account_id')
            ->where('sp_p_success',1)
            ->where('sp_s_id',$v_id)
            ->get();
    }

    function getAllStreams( $account_id = false, $owner_id = false, $expire = false, $check_paid = false, $not_paid_with_account_id = false, $pagination = 20, $page = 1 )
    {
        DB::enableQueryLog();
        $p_select = '';
        if($check_paid)
        {
            //$p_select = 'sp_payments.*,';
        }

        $qry = DB::table('sp_streams')->select(DB::raw($p_select.'sp_streams.*,sp_accounts.*,MD5(sp_accounts.sp_a_id) as md5_sp_a_id'));
        $qry->join('sp_accounts','sp_accounts.sp_a_id', '=', 'sp_streams.sp_s_host_id');
        if($account_id)
        {
            if(!$not_paid_with_account_id)
            {
                $qry->where('sp_a_id', $account_id);
            }
            if($check_paid)
            {
                //$qry->join('sp_payments','sp_accounts.sp_a_id', '=', 'sp_payments.sp_p_account_id', 'left');
            }
        }
        if($owner_id)
        {
            $qry->where('sp_s_host_id', $owner_id);
        }
        //TODO: time zone
        $qry->where('sp_s_stream_expire', '<', date('Y-m-d H:i:s'))->paginate($pagination,'*','page',$page);;

        $q = $qry->get();
        return $q;
    }

    function getAllHosts( $pagination = 20, $page = 1 )
    {
        $qry = DB::table('sp_accounts')->select(DB::raw('sp_accounts.*,MD5(sp_accounts.sp_a_id) as md5_sp_a_id'));
        $qry->where('sp_a_account_type', 2);
        $qry->where('sp_a_active', 1)->paginate($pagination,'*','page',$page);
        return $qry->get();
    }

    function saveStream( $data )
    {
        return DB::table('sp_streams')->insert($data);
    }

    function updateStream( $v_id, $data )
    {
        return DB::table('sp_streams')
            ->where('sp_s_stream_id', $v_id)
            ->update($data);
    }

    function getCategories()
    {
        return DB::table('sp_categories')->get();
    }

    function searchVideo( $search, $filters = [], $page = 1 )
    {
        $this->data = new Data();
        $terms = explode(' ',$search);
        $terms = array_map(function($value){
            return $this->data->cleanData($value);
        },$terms);

        $qry = DB::table('sp_streams')->where('sp_s_active',1);
        foreach($terms as $term)
        {
            $term = (string) $term;
            $qry->whereRaw(" ( sp_s_title LIKE '%$term%' OR sp_s_info LIKE '%$term%' ) ");
        }

        $filters = $this->data->cleanData($filters);
        if(!empty($filters))
        {
            ksort($filters);
            $filtered = [];
            foreach($filters as $k => $filter)
            {
                if(!empty($filter))
                {
                    $filtered[$k] = $filter;
                }
            }
            sort($filtered);
            $j = " JSON_CONTAINS(`sp_s_categories`, '". json_encode($filtered)."') ";
            $qry->whereRaw($j);
        }
        $qry->paginate(20,'*','page',$page);
        $q = $qry->get();
        return $q;
    }

    function searchHosts( $search, $filters = [], $page = 1 )
    {
        $this->data = new Data();
        $terms = explode(' ',$search);
        $terms = array_map(function($value){
            return $this->data->cleanData($value);
        },$terms);

        $qry = DB::table('sp_accounts');
        $qry->where('sp_a_account_type',2);
        $qry->where('sp_a_active',1);
        foreach($terms as $term)
        {
            $term = (string) $term;
            $qry->whereRaw(" ( sp_a_first_name LIKE '%$term%' OR sp_a_last_name LIKE '%$term%' OR sp_a_description LIKE '%$term%' OR sp_a_linkedin LIKE '%$term%'  ) ");
        }

        $filters = $this->data->cleanData($filters);
        if(!empty($filters))
        {
            ksort($filters);
            $filtered = [];
            foreach($filters as $k => $filter)
            {
                if(!empty($filter))
                {
                    $filtered[$k] = $filter;
                }
            }
            sort($filtered);
            $j = " JSON_CONTAINS(`sp_a_categories`, '". json_encode($filtered)."') ";
            $qry->whereRaw($j);
        }
        $qry->paginate(20,'*','page',$page);
        $q = $qry->get();
        return $q;
    }

}