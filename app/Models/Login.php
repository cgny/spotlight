<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class Login extends Model
{
    private $account, $session, $error, $data, $user_id;
    private                                    $salt1      = "ABCD", $salt2 = "EDFG";
    protected                                  $table      = 'sp_accounts';
    protected                                  $primaryKey = 'sp_a_id';

    function __construct( array $attributes = [] )
    {
        parent::__construct($attributes);
    }

    function login( $username, $password )
    {
        $this->account = new Account();
        $this->data    = new Data();
        $this->error   = new Error();

        $username = $this->data->cleanData($username);
        $password = $this->saltPassword($this->data->cleanData($password));

        if (!empty($user = $this->getUserData($username)))
        {
            if ($user->sp_a_password == $password)
            {
                //Auth::login($user);
                User::create([
                    'username' => $user->sp_a_id,
                    'email' => $user->sp_a_email,
                    'password' => Hash::make($user->sp_a_password),
                    'api_token' => Str::random(60),
                ]);

                return $this->account->updateAccount($user->sp_a_id, [ 'sp_a_last_login' => date("Y-m-d H:i:s", time()), 'sp_a_logged' => 1 ], false);
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    function getUserData( $username )
    {
        return DB::table($this->table)
            ->where('sp_a_username', $username)
            ->first();
    }

    function genPassword( $pass = null )
    {
        $salt1 = $this->salt1;
        $salt2 = $this->salt2;
        $rand  = rand(100000, 5000000);

        if ($pass)
        {
            $rand = $pass;
        }

        $pass                           = md5($salt1 . $rand . $salt2 . time());
        $return_pass['password']        = substr($pass, 0, 10);
        $return_pass['stored_password'] = substr(md5($salt1 . $return_pass['password'] . $salt2), 0, 20);
        return $return_pass;
    }

    function saltPassword( $password )
    {
        return substr(md5($this->salt1 . $password . $this->salt2), 0, 20);
    }

    function logout( $request )
    {
        if($account = $this->isLogged())
        {
            return false;
        }

        $this->account = new Account();
        $this->account->updateAccount($account->sp_a_id, [ 'sp_a_logged' => 0 ], false);
        $request->session()->flush();
    }

    function isLogged( $allow = false )
    {
        $this->account = new Account();
        if (!empty($this->account->getSessionUser()))
        {
            return $this->account->getSessionUser();
        }
        return false;
    }

}