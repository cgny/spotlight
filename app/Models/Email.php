<?php

namespace App\Models;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Email extends Mailable
{
    private $account, $session, $error, $data, $user_id, $email, $video;
    private                                                      $salt1      = "ABCD", $salt2 = "EDFG";
    protected                                                    $table      = 'sp_accounts';
    protected                                                    $primaryKey = 'sp_a_id';

    function getEmailTemplate( $id )
    {
        return DB::table('sp_emails')
            ->where('sp_e_id', $id)
            ->first();
    }

    function replaceEmailData( $message, $data )
    {
        if (isset($data['full_name']))
        {
            $message = str_replace('[NAME]', $data['full_name'], $message);
        }
        if (isset($data['email']))
        {
            $message = str_replace('[EMAIL]', $data['email'], $message);
        }
        if (isset($data['url']))
        {
            $message = str_replace('[URL]', $data['url'], $message);
        }
        return $message;
    }

    function sendEmailNotification( $email_type, $data )
    {
        $email   = $this->getEmailTemplate($email_type);
        $message = $this->replaceEmailData($email->sp_e_message, $data);

        try
        {
            $success = $this->mailer('sendmail')
                ->from('noreply-accounts@www.spotlight.com')
                ->to($data['email'],$data['full_name'])
                ->text($message);
            $err = false;
        }
        catch (\Exception $e)
        {
            $success = false;
            $err     = $e->getMessage();
        }
        return [ $success, $err ];
    }

}