<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 4/1/20
 * Time: 9:58 PM
 */


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Laravel\Cashier\Cashier;
use Illuminate\Support\Facades\Config;

class Stripe extends Model
{
    private $data, $login, $account, $error, $user_id, $key, $secret, $stripeClass;

    function __construct( array $attributes = [] )
    {
        parent::__construct($attributes);
        $environment   = App::environment();
        $this->key     = Config::get('constants.stripe.' . $environment . '.key');
        $this->secret  = Config::get('constants.stripe.' . $environment . '.secret');
        $this->user_id = session('user_id');
    }

    function createStripeCustomer( $email = "" )
    {
        $this->login       = new Login();
        $this->error       = new Error();
        $this->data        = new Data();
        $this->account     = new Account();
        $this->stripeClass = new \Stripe\Stripe();
        $this->stripeClass->setApiKey($this->secret);

        $account = $this->account->getAccountById($this->login->isLogged()->sp_a_id);
        if (empty($account))
        {
            return false;
        }

        if (empty($email))
        {
            $email = $account->sp_a_email;
        }

        $description = $account->sp_a_first_name . ' ' . $account->sp_a_last_name;
        $customer    = false;

        try
        {
            if (!empty($account->sp_a_stripe_id))
            {
                //update customer
                $cu              = \Stripe\Customer::retrieve($account->sp_a_stripe_id);
                $cu->description = $description;
                $cu->email       = $email;
                $cu->save();
            }
            else
            {
                //create customer
                $cust     = new \Stripe\Customer();
                $customer = $cust->create(
                    [
                        'email'       => $email,
                        'description' => $description
                    ]

                );

                $this->account->updateAccount($account->sp_a_id,
                    [ 'sp_a_stripe_id' => $customer->id ]
                );
            }
            $success = true;
            $error   = false;
        }
        catch (\Exception $e)
        {
            $success = $customer = false;
            $error   = $e->getMessage();
            $data    = [
                'file'     => __FILE__,
                'line'     => __LINE__,
                'function' => __FUNCTION__
            ];
            $this->error->writeLog('error', $e->getMessage(), $data);
        }
        return array(
            "customer" => $customer,
            "success"  => $success,
            "error"    => $error
        );
    }

    function createStripeAccount( $email, $ssn_last_4 = "" )
    {
        $this->login       = new Login();
        $this->account     = new Account();
        $this->error       = new Error();
        $this->data        = new Data();
        $this->stripeClass = new \Stripe\Stripe();
        $this->stripeClass->setApiKey($this->secret);

        $account = $this->account->getAccountById($this->login->isLogged()->sp_a_id);

        $ssn_last_4 = (empty($ssn_last_4)) ? null : $ssn_last_4;

        $acct       = new \Stripe\Account;
        $acct_array = array(
            'email'                  => $email,
            'country'                => $account->sp_a_country,
            'type'                   => 'custom',
            'business_type'          => 'individual',
            'business_name'          => $account->sp_a_business_name,
            'business_url'           => $account->sp_a_business_url,
            'requested_capabilities' => [ 'transfers' ],
            'individual'             => array(
                'first_name'      => $account->sp_a_first_name,
                'last_name'       => $account->sp_a_last_name,
                'ssn_last_4'      => $ssn_last_4,
                'phone'           => $account->sp_a_phone,
                'address'         => array(
                    'line1'       => $account->sp_a_address_1,
                    'line2'       => $account->sp_a_address_2,
                    'city'        => $account->sp_a_city,
                    'state'       => $account->sp_a_state,
                    'postal_code' => $account->sp_a_postal_code,
                ),
                'dob'             => array(
                    'month' => $account->sp_a_dob_m,
                    'day'   => $account->sp_a_dob_d,
                    'year'  => $account->sp_a_dob_y,
                ),
                'business_tax_id' => $account->sp_a_tax_id,
                'business_vat_id' => $account->sp_a_vat_id,

            ),
        );

        try
        {
            $new_account = $acct->create(
                $acct_array
            );

            $acct                       = \Stripe\Account::retrieve($new_account->id);
            $acct->tos_acceptance->date = time();
            $acct->tos_acceptance->ip   = $_SERVER['REMOTE_ADDR'];
            $acct->save();

            $this->account->updateAccount($account->sp_a_id,
                array(
                    'sp_a_stripe_account_id' => $new_account->id
                )
            );
            $success = true;
            $error   = false;
        }
        catch (Exception $e)
        {
            $success = $account = false;
            $error   = $e->getMessage();
            $data    = [
                'file'     => __FILE__,
                'line'     => __LINE__,
                'function' => __FUNCTION__
            ];
            $this->error->writeLog('error', $e->getMessage(), $data);
        }

        return array(
            "account" => $account,
            "success" => $success,
            "error"   => $error,
            "acct"    => $acct_array
        );
    }

    function updateStripeAccount( $account_id )
    {
        $this->account     = new Account();
        $this->error       = new Error();
        $this->data        = new Data();
        $this->stripeClass = new \Stripe\Stripe();
        $this->stripeClass->setApiKey($this->secret);

        $account = $this->account->getAccountById($account_id);

        try
        {
            $acct = \Stripe\Account::retrieve($account->sp_a_stripe_account_id);
        }
        catch (Exception $e)
        {
            throw new Exception("Account Not Found");
        }

        try
        {
            $acct->email = $account->sp_a_email;

            $acct->business_profile->name = $account->sp_a_business_name;
            if ($account->sp_a_business_url)
            {
                $acct->business_profile->url = $account->sp_a_business_url;
            }
            else
            {
                $acct->business_profile->url = 'www.spotlight.com';
            }
            if ($account->sp_a_support_email)
            {
                $acct->business_profile->support_email = $account->sp_a_support_email;
            }
            if ($account->sp_a_support_phone)
            {
                $acct->business_profile->support_phone = $account->sp_a_support_phone;
            }
            if ($account->sp_a_support_url)
            {
                $acct->business_profile->support_url = $account->sp_a_support_url;
            }
            if ($account->sp_a_industry)
            {
                $acct->business_profile->mcc = $account->sp_a_industry;
            }
            //$acct->legal_entity->business_tax_id = $account->sp_a_tax_id;

            if ($account->sp_a_email)
            {
                $acct->individual->email = $account->sp_a_email;
            }
            if ($account->sp_a_dob_m)
            {
                $acct->individual->dob->month = $account->sp_a_dob_m;
            }
            if ($account->sp_a_dob_d)
            {
                $acct->individual->dob->day = $account->sp_a_dob_d;
            }
            if ($account->sp_a_dob_y)
            {
                $acct->individual->dob->year = $account->sp_a_dob_y;
            }
            if ($account->sp_a_address_1)
            {
                $acct->individual->address->line1 = $account->sp_a_address_1;
            }
            if ($account->sp_a_address_2)
            {
                $acct->individual->address->line2 = $account->sp_a_address_2;
            }
            if ($account->sp_a_city)
            {
                $acct->individual->address->city = $account->sp_a_city;
            }
            if ($account->sp_a_state)
            {
                $acct->individual->address->state = $account->sp_a_state;
            }
            if ($account->sp_a_postal_code)
            {
                $acct->individual->address->postal_code = $account->sp_a_postal_code;
            }
            if ($account->sp_a_country)
            {
                $acct->individual->address->country = $account->sp_a_country;
            }
            if ($account->sp_a_currency)
            {
                $acct->default_currency = $account->sp_a_currency;
            }

            $acct->save();

            $account = true;
            $success = true;
            $error   = false;
        }
        catch (Exception $e)
        {
            $error = $e->getMessage();
            $data  = [
                'file'     => __FILE__,
                'line'     => __LINE__,
                'function' => __FUNCTION__
            ];
            $this->error->writeLog('error', $e->getMessage(), $data);
            $success = $account = false;
        }

        return array(
            "account" => $account,
            "success" => $success,
            "error"   => $error
        );
    }

    function authorizeAccount( $code )
    {
        $this->account = new Account();
        $this->login   = new Login();

        if (empty($code))
        {
            return;
        }
        $ch = curl_init("https://connect.stripe.com/oauth/token?client_secret=" . $this->secret . "&code=$code&grant_type=authorization_code");
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);

        $data = json_decode($output);

        if (isset($data->error_description))
        {
            echo $data->error_description;
            //exit;
        }

        $this->account->updateAccount($this->login->isLogged(),
            array(
                'stripe_user_id'       => $data->stripe_user_id,
                'stripe_refresh_token' => $data->refresh_token,
                'stripe_access_token'  => $data->access_token
            )
        );

    }

    function addCustomerCard( $acct_info, $data )
    {
        $this->stripeClass = new \Stripe\Stripe;
        $this->stripeClass->setApiKey($this->secret);

        $result = [];
        if (empty($data['pm_id']))
        {
            $result['success']          = false;
            $result['error']['message'] = 'Data failure';
            $result['error']['code']    = 105;
            return $result;
        }

        $c = DB::table('sp_cards')->where('sp_ca_stripe_id', $data['pm_id'])->get();
        if (count($c) > 0)
        {
            $result['success']          = false;
            $result['error']['message'] = 'Card Exists';
            $result['error']['code']    = 120;
            return $result;
        }

        if($data['s_default'] == 1)
        {
            DB::table('sp_cards')->where('sp_a_id', $acct_info->sp_a_id)->update(['sp_ca_default' => 0]);
        }

        try
        {
            $stripe = new \Stripe\PaymentMethod();
            $pm     = $stripe->retrieve(
                $data['pm_id'],
                []
            );

            $c_id = DB::table('sp_cards')->insert(
                [
                    'sp_a_id'         => $acct_info->sp_a_id,
                    'sp_ca_name'      => $data['saved_name'],
                    'sp_ca_type'      => $pm->card->brand,
                    'sp_ca_last_4'    => $pm->card->last4,
                    'sp_ca_month'     => $pm->card->exp_month,
                    'sp_ca_year'      => $pm->card->exp_year,
                    'sp_ca_stripe_id' => $data['pm_id'],
                    'sp_ca_intent_id' => $data['si_id'],
                    'sp_ca_default'   => $data['s_default']
                ]
            );

            $result['success']          = true;
            $result['error']['message'] = false;
            $result['error']['code']    = false;

        }
        catch (\Stripe\InvalidRequestError $e)
        {
            // Invalid parameters were supplied to Stripe's API
            $result['error']['message'] = $e->getMessage();
            $result['error']['code']    = $e->getCode();

        }
        catch (\Stripe\AuthenticationError $e)
        {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $result['error']['message'] = $e->getMessage();
            $result['error']['code']    = $e->getCode();

        }
        catch (\Stripe\ApiConnectionError $e)
        {
            // Network communication with Stripe failed
            $result['error']['message'] = $e->getMessage();
            $result['error']['code']    = $e->getCode();

        }
        catch (\Error $e)
        {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $result['error']['message'] = $e->getMessage();
            $result['error']['code']    = $e->getCode();

        }
        catch (Exception $e)
        {
            // Something else happened, completely unrelated to Stripe
            $result['error']['message'] = $e->getMessage();
            $result['error']['code']    = $e->getCode();
        }
        return $result;
    }

    function addAccountBank( $account, $stripeToken, $data )
    {
        $this->error = new Error();

        $this->stripeClass = new \Stripe\Stripe;
        $this->stripeClass->setApiKey($this->secret);
        $this->error = new Error();
        $acct        = \Stripe\Account::retrieve($account->sp_a_stripe_account_id);


        try
        {
            $r = \Stripe\Account::createExternalAccount(
                $acct->id,
                [
                    'external_account' => $stripeToken,
                ]
            );

            $acct->save();

            $acct    = \Stripe\Account::retrieve($account->sp_a_stripe_account_id);
            $bank_id = $acct->external_accounts->data[($acct->external_accounts->total_count - 1)]->id;

            $data['sp_b_stripe_id'] = $bank_id;
            $b_id                   = DB::table('sp_banks')->insert($data);

            $account = true;
            $success = true;
            $error   = false;

        }
        catch (Exception $e)
        {
            $error = $e->getMessage();
            $data  = [
                'file'     => __FILE__,
                'line'     => __LINE__,
                'function' => __FUNCTION__
            ];
            $this->error->writeLog('error', $e->getMessage(), $data);
            $success = $account = false;
        }

        return array(
            "account" => $account,
            "success" => $success,
            "error"   => $error
        );
    }

    function setDefaultCard( object $card )
    {
        $this->stripeClass = new \Stripe\Stripe;
        $this->stripeClass->setApiKey($this->secret);

        $this->account    = new Account();
        $account          = $this->account->getAccountById($card->sp_a_id);
        $cu               = \Stripe\Customer::retrieve($account->sp_a_stripe_id);
        $cu->default_card = $card->sp_c_stripe_id;
        $cu->save();
    }

    function createIntent( $account_id )
    {
        $this->stripeClass = new \Stripe\Stripe;
        $this->stripeClass->setApiKey($this->secret);
        $this->account = new Account();
        $account       = $this->account->getAccountById($account_id);
        $cu            = \Stripe\Customer::retrieve($account->sp_a_stripe_id);
        return \Stripe\SetupIntent::create([
                'customer' => $cu->id
            ]
        );
    }

    function removeCard()
    {

    }

    function setDefaultBank( object $bank )
    {
        $this->stripeClass = new \Stripe\Stripe;
        $this->stripeClass->setApiKey($this->secret);

        $this->account = new Account();
        $account       = $this->account->getAccountById($bank->sp_a_id);
        \Stripe\Account::updateExternalAccount($account->sp_a_stripe_account_id, $bank->sp_b_stripe_id, [
                'default_for_currency' => true
            ]
        );
    }

    function removeBank()
    {

    }

    function chargeCard()
    {


    }

}