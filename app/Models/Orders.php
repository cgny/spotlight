<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 4/1/20
 * Time: 9:58 PM
 */


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Laravel\Cashier\Cashier;

class Orders extends Model
{
    private $order;

    function __construct( array $attributes = [] )
    {
        parent::__construct($attributes);
        return $this;
    }

    function getOrder()
    {
        return $this->order;
    }

    function setOrder( $cart_id )
    {
        $this->order = DB::table('sp_payments')
            ->join('sp_accounts', 'sp_accounts.account_id', '=', 'sp_payments.sp_a_account_id')
            ->where('sp_p_cart_id', $cart_id)
            ->get();
        return $this->order;
    }

    function findOrder( $cart_id )
    {
        return DB::table('sp_payments')
            ->join('sp_accounts', 'sp_accounts.account_id', '=', 'sp_payments.sp_a_account_id')
            ->where('sp_p_cart_id', $cart_id)
            ->get();
    }

    function addStream( $cart_id, $stream_id )
    {
        $data = [
            'sp_p_cart_id'   => $cart_id,
            'sp_p_stream_id' => $stream_id
        ];
        return DB::table('sp_payments')
            ->insert($data);
    }

    function removeStream( $cart_id, $stream_id )
    {
        return DB::table('sp_payments')
            ->where('sp_p_cart_id', $cart_id)
            ->where('sp_p_stream_id', $stream_id)
            ->delete();
    }


}