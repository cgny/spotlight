<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Log;


class Error extends Model
{

    function writeLog( $type = "debug", $info = false, $data = [])
    {
        Log::$type($info,$data);
        /*
         * alert
         * critical
         * error
         * warning
         * notice
         * info
         * debug
         */
    }

    function logQuery()
    {
        DB::enableQueryLog();
    }

    function showQuery()
    {
        dd(DB::getQueryLog());
    }

}