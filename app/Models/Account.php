<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;


class Account extends Model
{
    private $data, $login, $error, $request, $stripe, $user_id, $key, $secret, $stripeClass;
    protected                                                                  $table      = 'sp_accounts';
    protected                                                                  $primaryKey = 'sp_a_id';

    function __construct( array $attributes = [] )
    {
        parent::__construct($attributes);
        $environment  = App::environment();
        $this->key    = Config::get('Constants.stripe.' . $environment . '.key');
        $this->secret = Config::get('Constants.stripe.' . $environment . '.secret');
    }

    function authenticateAccount( $auth_code, $data )
    {
        return DB::table('sp_accounts')
            ->where('sp_a_id', $auth_code)
            ->update(array(
                    'stripe_card_num'  => $data['last4'],
                    'stripe_card_id'   => $data['card_id'],
                    'stripe_card_type' => $data['type']
                )
            );
    }

    function setSessionUser( $user )
    {
        session([ 'user' => $user, 'session_id' => session_id() ]);
        return $this;
    }

    function getSessionUser()
    {
        if (!empty(session('user')))
        {
            return session('user');
        }
        return false;
    }

    function getSessionId()
    {
        return session('session_id');
    }

    function getUser( $user_id = false )
    {
        $this->data = new Data();
        $data       = $this->getSessionUser();
        if (empty($data))
        {
            return false;
        }
        if (empty($user_id))
        {
            $user_id = $data->sp_a_id;
        }

        $user_id = $this->data->cleanData($user_id);
        $user    = DB::table('sp_accounts')
            ->where('sp_a_id', $user_id)
            ->first();
        $this->setSessionUser($user);
        return $user;
    }

    function getAllAccounts( $limit = false )
    {
        $this->data = new Data();
        if ($limit)
        {
            $limit = $this->data->cleanData($limit);
        }
        $user = DB::table('sp_accounts')->select(DB::raw('sp_accounts.*,MD5(sp_accounts.sp_a_id) as md5_sp_a_id'))
            ->limit($limit)
            ->orderBy('sp_a_id', "desc");
        return $user;
    }

    function saveAccount( $user )
    {
        $this->stripe = new Stripe();
        $login        = new Login();
        $pass         = $login->genPassword();
        if ($this->checkEmail($user['email']) > 0)
        {
            return false;
        }

        $this->stripeClass = new \Stripe\Stripe;
        $this->stripeClass->setApiKey($this->secret);
        $account = array(
            "sp_a_first_name"      => $user['first_name'],
            "sp_a_last_name"       => $user['last_name'],
            "sp_a_username"        => $user['username'],
            "sp_a_password"        => $pass['stored_password'],
            "sp_a_key"             => substr(md5(rand(1, 9999999)), 0, 10),
            "sp_a_activation_code" => $user['code'],
            "sp_a_phone"           => $user['phone'],
            "sp_a_email"           => $user['email'],
            "sp_a_photo"           => $user['photo'],
            "sp_a_timezone"        => $user['time_zome'],
            "sp_a_currency"        => $user['currency'],
            "sp_a_country"         => $user['country'],
            "sp_a_account_type"    => $user['type'],
            "sp_a_description"     => $user['description'],
            "sp_a_last_login"      => gmdate("Y-m-d H:i:s"),
            "sp_a_created"         => gmdate("Y-m-d H:i:s")
        );
        $ins = DB::table('sp_accounts')->insert($account);
        $this->stripe->createStripeCustomer($user['email']);
        $this->stripe->createStripeAccount($user['email']);
        $this->user_id = $ins;

        return [ $ins, $pass['password'] ];
    }

    function activateAccount( $sp_a_activation_coded )
    {
        return DB::table('sp_accounts')
            ->where('sp_a_activation_code', $sp_a_activation_coded)
            ->update(array(
                    'sp_a_active' => 1
                )
            );
    }

    function addAccountBank( $account, $stripeToken, $data )
    {
        $this->stripe = new Stripe();
        return $this->stripe->addAccountBank($account, $stripeToken, $data);
    }

    function addCustomerCard( $acct_info, $data )
    {
        $this->stripe = new Stripe();
        return $this->stripe->addCustomerCard($acct_info, $data);
    }

    function genPassword( $pass = null )
    {
        $salt1 = "ABCD";
        $salt2 = "EDFG";
        $rand  = rand(100000, 5000000);
        if ($pass)
        {
            $rand = $pass;
        }
        $pass = md5($salt1 . $rand . $salt2 . time());
        return $pass;
    }

    function sendData( $data )
    {

    }

    function getDayDifference( $to, $from )
    {
        $date1    = new DateTime($to);
        $date2    = new DateTime($from);
        $interval = $date1->diff($date2);
        return number_format($interval->days, 0, '', '');
    }

    function getAccountById( $account_id )
    {
        $this->data = new Data();
        $account_id = $this->data->cleanData($account_id);
        return DB::table('sp_accounts')->select('*')->where('sp_a_id', '=', $account_id)->first();
    }

    function getAccountByHashId( $account_id )
    {
        $this->data = new Data();
        $account_id = $this->data->cleanData($account_id);
        $q          = DB::table('sp_accounts')->select(DB::raw('sp_accounts.*,MD5(sp_accounts.sp_a_id) as md5_sp_a_id'))->where(DB::raw('MD5(sp_a_id)'), $account_id)->first();
        return $q;
    }

    function updateAccount( $account_id, $data, $stripe = true )
    {
        $this->data   = new Data();
        $this->stripe = new Stripe();
        $account_id = $this->data->cleanData($account_id);
        $data       = $this->data->cleanData($data);

        $upd = DB::table('sp_accounts')
            ->where('sp_a_id', $account_id)
            ->update($data);

        $account = $this->getAccountById($account_id);

        if ($stripe == true && empty($account->sp_a_stripe_id))
        {

            $this->stripe->createStripeCustomer($account->sp_a_email);
        }

        if ($stripe == true && !empty($account->sp_a_stripe_account_id))
        {

            $this->stripe->updateStripeAccount($account_id);
        }

        $this->setSessionUser($account);

        return $upd;
    }

    function getMySales($account_id)
    {
        $this->login = new Login();
        $qry         = sprint_f("SELECT * FROM sp_streams as s
                                JOIN sp_payments as p ON ( s.sp_s_stream_id = p.sp_p_stream_id ) 
                                WHERE sp_s_host_id = %s", $account_id
        );
        $sales       = DB::select(DB::raw($qry));
        return $sales;
    }

    function getMyOrders()
    {
        $this->login = new Login();
        $qry         = sprint_f("SELECT * FROM sp_payments WHERE sp_p_account_id = %s", $account_id);
        $sales       = DB::select(DB::raw($qry));
        return $sales;
    }

    function getAllSales()
    {
        $this->login = new Login();
        $qry         = "SELECT * FROM sp_streams as s JOIN sp_payments as p ON ( s.sp_s_stream_id = p.sp_p_stream_id )";
        $sales       = DB::select(DB::raw($qry));
        return $sales;
    }

    function checkForCard()
    {
        $this->login = new Login();
        $card        = null;
        if ($account = $this->login->isLogged())
        {
            $card = ($account->sp_a_stripe_card_id);
        }
        return json_encode([ 'card' => $card ]);
    }

    function getBankAccounts( $account_id )
    {
        return DB::table('sp_banks')->where('sp_a_id', $account_id)->orderBy('sp_b_id', 'DESC')->get();
    }

    function getCards( $account_id )
    {
        return DB::table('sp_cards')->where('sp_a_id', $account_id)->orderBy('sp_ca_id', 'DESC')->get();
    }

    function checkEmail( $email )
    {
        return DB::table($this->table)->where('sp_a_email', $email)->count();
    }

    function checkUsername( $username )
    {
        return DB::table($this->table)->where('sp_a_username', $username)->count();
    }

    function setAsDefaultCard( $id )
    {
        $this->stripe = new Stripe();
        $card = $this->getCardByStripeId($id);
        if($card)
        {
            DB::table('sp_cards')->where('sp_a_id', $card->sp_a_id)->update([ 'sp_ca_default' => 0 ]);
            DB::table('sp_cards')
                ->where('sp_ca_id', $card->sp_ca_id)
                ->update([ 'sp_ca_default' => 1 ]);
            //$this->stripe->setDefaultCard( $card );
            return $this->getCards($card->sp_a_id);
        }
        return [];
    }

    function getCardByStripeId( $id )
    {
        return DB::table('sp_cards')->where('sp_ca_stripe_id', "pm_$id")->first();
    }

    function removeCard( $id )
    {
        $this->stripe = new Stripe();
        $card = $this->getCardByStripeId($id);
        if($card)
        {
            DB::table('sp_cards')->where('sp_ca_stripe_id', "pm_$id")->delete();
            DB::table('sp_cards')
                ->where('sp_a_id', $card->sp_a_id)
                ->limit(1)
                ->orderBy('sp_ca_id', 'desc')
                ->update([ 'sp_ca_default' => 1 ]);
            //$this->stripe->removeCard();
            //$this->stripe->setDefaultCard( $this->getDefaultCard( $card->sp_a_id ) );
            return $this->getCards($card->sp_a_id);
        }
        return [];
    }

    function getDefaultCard( $account_id )
    {
        return DB::table('sp_cards')
            ->where('sp_a_id', $account_id)
            ->where('sp_ca_default', 1)
            ->first();
    }

    function setAsDefaultBank( $id )
    {
        $this->stripe = new Stripe();
        $bank = $this->getBankByStripeId($id);
        if($bank)
        {
            DB::table('sp_banks')->where('sp_a_id', $bank->sp_a_id)->update([ 'sp_b_default' => 0 ]);
            DB::table('sp_banks')
                ->where('sp_b_id', $bank->sp_b_id)
                ->update([ 'sp_b_default' => 1 ]);
            $this->stripe->setDefaultBank($bank);
            return $this->getBankAccounts($bank->sp_a_id);
        }
        return [];
    }

    function getBankByStripeId( $id )
    {
        $q = DB::table('sp_banks')->where('sp_b_stripe_id', "ba_$id")->first();
        return $q;
    }

    function removeBank( $id )
    {
        $this->stripe = new Stripe();
        $bank = $this->getBankByStripeId($id);
        if($bank)
        {
            DB::table('sp_banks')->where('sp_b_stripe_id', "ba_$id")->delete();
            DB::table('sp_banks')
                ->where('sp_a_id', $bank->sp_a_id)
                ->limit(1)
                ->orderBy('sp_b_id', 'desc')
                ->update([ 'sp_b_default' => 1 ]);
            $this->stripe->removeBank();
            $this->stripe->setDefaultBank( $this->getDefaultBank( $bank->sp_a_id ) );
            return $this->getBankAccounts($bank->sp_a_id);
        }
        return [];
    }

    function getDefaultBank( $account_id )
    {
        return DB::table('sp_banks')
            ->where('sp_a_id', $account_id)
            ->where('sp_b_default', 1)
            ->first();
    }
}
