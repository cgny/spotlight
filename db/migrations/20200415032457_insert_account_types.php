<?php

use Phinx\Migration\AbstractMigration;

class InsertAccountTypes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */

    function up()
    {

        $rows = [
            [
                'sp_at_id'    => 1,
                'sp_at_name'  => 'Viewer'
            ],
            [
                'sp_at_id'    => 2,
                'sp_at_name'  => 'Host'
            ]
        ];
        $this->table('sp_account_types')->insert($rows)->save();

        $rows = [
            [
                'sp_as_id'    => 1,
                'sp_as_name'  => 'Not Active'
            ],
            [
                'sp_as_id'    => 2,
                'sp_as_name'  => 'Active'
            ],
            [
                'sp_as_id'    => 3,
                'sp_as_name'  => 'Inactive'
            ],
            [
                'sp_as_id'    => 4,
                'sp_as_name'  => 'Pending'
            ]
        ];
        $this->table('sp_account_status')->insert($rows)->save();

        $rows = [
            [
                'sp_e_id'    => 1,
                'sp_e_title'  => 'Account Activation',
                'sp_e_message'  => '
                Welcome to Hotlight, [NAME] 
                
                Email: [EMAIL]
                Please activation your account by clicking here : [URL]
                
                Hotlight
                NYC, NY
                contact@hotlight.com',
            ],
            [
                'sp_e_id'    => 2,
                'sp_e_title'  => 'Password Reset',
                'sp_e_message'  => '
                Hotlight
                
                Password reset request for your Hotlight account. 
                
                Email: [EMAIL]
                Password: [PASSWORD]
                Please activation your account by clicking here : [URL]
                
                Hotlight
                NYC, NY
                contact@hotlight.com',
            ],
            [
                'sp_e_id'    => 3,
                'sp_e_title'  => 'Hotlight Stream Receipt',
                'sp_e_message'  => '
                Hotlight
                
                You have purchased a Hotlight stream. 
                
                Stream Name: [STREAM_NAME]
                Scheduled Time: [STREAM_DATE]
                Stream Length: [STREAM_LENGTH]
                Accessible: [ACCESS_TIME]
                Live: [LIVE_STREAM]
                
                Video Stream URL : [URL]
                
                Hotlight
                NYC, NY
                contact@hotlight.com',
            ],
            [
                'sp_e_id'    => 4,
                'sp_e_title'  => 'Hotlight Stream Notification (Starting)',
                'sp_e_message'  => '
                Hotlight
                
                Your stream will be starting soon.
                
                Stream Name: [STREAM_NAME]
                Scheduled Time: [STREAM_DATE]
                Stream Length: [STREAM_LENGTH]
                Accessible Until: [ACCESS_TIME]
                Live: [LIVE_STREAM]
                
                Video Stream URL : [URL]
                
                Hotlight
                NYC, NY
                contact@hotlight.com',
            ],
            [
                'sp_e_id'    => 5,
                'sp_e_title'  => 'Hotlight Stream Notification (Ended)',
                'sp_e_message'  => '
                Hotlight
                
                Your subscribed stream has ended.
                
                Stream Name: [STREAM_NAME]
                Scheduled Time: [STREAM_DATE]
                Accessible Until: [ACCESS_TIME]
                Downloadable: [DOWNLOAD_URL]                
                
                
                Hotlight
                NYC, NY
                contact@hotlight.com',
            ],
        ];
        $this->table('sp_emails')->insert($rows)->save();
    }

    function down()
    {

    }
}
