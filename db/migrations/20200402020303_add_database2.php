<?php

use Phinx\Migration\AbstractMigration;

class AddDatabase2 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // create the table
        $table = $this->table('sp_errors', [ 'id' => 'sp_e_id' ]);
        $table->addColumn('sp_e_file', 'string', [ 'limit' => 200 ])
            ->addColumn('sp_e_line', 'string', [ 'limit' => 200 ])
            ->addColumn('sp_e_function', 'string', [ 'limit' => 200])
            ->addColumn('sp_e_error', 'string', [ 'limit' => 100 ])
            ->addColumn('sp_e_query', 'string', [ 'limit' => 100, 'null' => true ])
            ->addColumn('sp_e_data', 'string', [ 'limit' => 100, 'null' => true ])
            ->addIndex(['sp_e_id'], ['unique' => true])
            ->create();

        // create the table
        $table = $this->table('sp_video_conference_log', [ 'id' => 'sp_vc_id' ]);
        $table->addColumn('sp_vc_account_id', 'string', [ 'limit' => 200 ])
            ->addColumn('sp_vc_room_id', 'string', [ 'limit' => 200])
            ->addColumn('sp_vc_start', 'datetime')
            ->addColumn('sp_vc_end', 'datetime')
            ->addIndex(['sp_vc_id'], ['unique' => true])
            ->create();

        // create the table
        $table = $this->table('sessions', [ 'id' => 'id' ]);
        $table->addColumn('user_id', 'integer', [ 'signed' => false, 'null' => true ])
            ->addColumn('ip_address', 'string', [ 'limit' => 45, 'null' => true ])
            ->addColumn('user_agent', 'text', [ 'null' => true ])
            ->addColumn('payload', 'text')
            ->addColumn('last_activity', 'integer')
            ->addIndex(['id'], ['unique' => true])
            ->create();

    }
}
