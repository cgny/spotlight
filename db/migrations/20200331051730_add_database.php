<?php

use Phinx\Migration\AbstractMigration;

class AddDatabase extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // create the table
        $table = $this->table('sp_accounts', [ 'id' => 'sp_a_id' ]);
        $table->addColumn('sp_a_first_name', 'string', [ 'limit' => 200 ])
            ->addColumn('sp_a_last_name', 'string', [ 'limit' => 200 ])
            ->addColumn('sp_a_email', 'string', [ 'limit' => 200 ])
            ->addColumn('sp_a_phone', 'string', [ 'limit' => 200 ])
            ->addColumn('sp_a_username', 'string', [ 'limit' => 100 ])
            ->addColumn('sp_a_password', 'string', [ 'limit' => 100 ])
            ->addColumn('sp_a_currency', 'string', [ 'limit' => 200, 'default' => 'USD' ])
            ->addColumn('sp_a_description', 'string', [ 'limit' => 200, 'null' => true ])
            ->addColumn('sp_a_business_name', 'string', [ 'limit' => 200, 'null' => true ])
            ->addColumn('sp_a_business_url', 'string', [ 'limit' => 200, 'null' => true ])
            ->addColumn('sp_a_address_1', 'string', [ 'limit' => 200, 'null' => true ])
            ->addColumn('sp_a_address_2', 'string', [ 'limit' => 200, 'null' => true ])
            ->addColumn('sp_a_city', 'string', [ 'limit' => 100, 'null' => true ])
            ->addColumn('sp_a_state', 'string', [ 'limit' => 100, 'null' => true ])
            ->addColumn('sp_a_postal_code', 'string', [ 'limit' => 12, 'null' => true ])
            ->addColumn('sp_a_tax_id', 'string', [ 'limit' => 200, 'null' => true ])
            ->addColumn('sp_a_dob_m', 'integer', [ 'limit' => 200, 'null' => true ])
            ->addColumn('sp_a_dob_d', 'integer', [ 'limit' => 200, 'null' => true ])
            ->addColumn('sp_a_dob_y', 'integer', [ 'limit' => 200, 'null' => true ])
            ->addColumn('sp_a_timezone', 'string', [ 'limit' => 100, 'default' => 'America/New_York' ])
            ->addColumn('sp_a_photo', 'string', [ 'limit' => 100, 'null' => true ])
            ->addColumn('sp_a_ig', 'string', [ 'limit' => 200, 'null' => true ])
            ->addColumn('sp_a_linkedin', 'string', [ 'limit' => 200, 'null' => true ])
            ->addColumn('sp_a_account_type', 'boolean', [ 'default' => 0 ]) //0 -> viewer , 1 -> host
            ->addColumn('sp_a_status', 'boolean', [ 'default' => 4 ]) //0, 1, 2
            ->addColumn('sp_a_active', 'boolean', [ 'default' => 0 ])
            ->addColumn('sp_a_key', 'string', [ 'limit' => 100 ])
            ->addColumn('sp_a_stripe_id', 'string', [ 'limit' => 100, 'null' => true ])
            ->addColumn('sp_a_stripe_account_id', 'string', [ 'limit' => 100, 'null' => true ])
            ->addColumn('sp_a_stripe_card_type', 'integer', [ 'null' => true ])
            ->addColumn('sp_a_stripe_card_id', 'string', [ 'limit' => 100, 'null' => true ])
            ->addColumn('sp_a_stripe_last_4', 'integer', [ 'null' => true ])
            ->addColumn('sp_a_google_calendar_access_token', 'text', [ 'null' => true ])
            ->addColumn('sp_a_google_calendar_refresh_token', 'text', [ 'null' => true ])
            ->addColumn('sp_a_created', 'timestamp', [ 'timezone' => true, 'default' => 'CURRENT_TIMESTAMP' ])
            ->addColumn('sp_a_activation_code', 'string', [ 'limit' => 100 ])
            ->addColumn('sp_a_activated', 'datetime', [ 'null' => true ])
            ->addColumn('sp_a_last_login', 'datetime', [ 'null' => true ])
            ->addIndex([ 'sp_a_id', 'sp_a_email', 'sp_a_username' ], [ 'unique' => true ])
            ->create();

        $table = $this->table('sp_account_types', [ 'id' => 'sp_at_id' ]);
        $table->addColumn('sp_at_name', 'string', [ 'limit' => 200 ])
            ->addIndex([ 'sp_at_id' ])
            ->create();

        $table = $this->table('sp_account_status', [ 'id' => 'sp_as_id' ]);
        $table->addColumn('sp_as_name', 'string', [ 'limit' => 200 ])
            ->addIndex([ 'sp_as_id' ])
            ->create();

        $table = $this->table('sp_streams', [ 'id' => 'sp_s_id' ]);
        $table->addColumn('sp_s_stream_id', 'string', [ 'limit' => 200 ])
            ->addColumn('sp_s_host_id', 'string', [ 'limit' => 200 ])
            ->addColumn('sp_s_image', 'string', [ 'limit' => 200 ])
            ->addColumn('sp_s_name', 'string', [ 'limit' => 200 ])
            ->addColumn('sp_s_info', 'text', [ 'null' => true ])
            ->addColumn('sp_s_active', 'boolean', [ 'default' => 0 ])
            ->addColumn('sp_s_rate', 'decimal', [ 'default'   => 0,
                                                  'precision' => 10,
                                                  'scale'     => 2 ]
            )
            ->addColumn('sp_s_length', 'integer', [ 'default' => 900 ])//15min
            ->addColumn('sp_s_access_length', 'integer', [ 'default' => 900 ])//15min
            ->addColumn('sp_s_stream_date', 'timestamp', [ 'timezone' => true ])
            ->addColumn('sp_s_created', 'timestamp', [ 'timezone' => true, 'default' => 'CURRENT_TIMESTAMP' ])
            ->addColumn('sp_s_updated', 'timestamp', [ 'timezone' => true ])
            ->addIndex([ 'sp_s_id', 'sp_s_stream_id' ], [ 'unique' => true ])
            ->addIndex([ 'sp_s_host_id' ])
            ->create();

        $table = $this->table('sp_payments', [ 'id' => 'sp_p_id' ]);
        $table->addColumn('sp_p_account_id', 'string', [ 'limit' => 200 ])
            ->addColumn('sp_p_cart_id', 'string', [ 'limit' => 200 ])
            ->addColumn('sp_p_stream_id', 'integer')
            ->addColumn('sp_p_amount', 'string', [ 'limit' => 200 ])
            ->addColumn('sp_p_success', 'boolean', [ 'default' => 0 ])
            ->addColumn('sp_p_created', 'timestamp', [ 'timezone' => true, 'default' => 'CURRENT_TIMESTAMP' ])
            ->addIndex([ 'sp_p_id' ], [ 'unique' => true ])
            ->addIndex([ 'sp_p_account_id', 'sp_p_stream_id' ])
            ->create();

        $table = $this->table('sp_messages', [ 'id' => 'sp_m_id' ]);
        $table->addColumn('sp_m_account_id', 'string', [ 'limit' => 200 ])
            ->addColumn('sp_m_stream_id', 'integer')
            ->addColumn('sp_m_message', 'string', [ 'limit' => 200 ])
            ->addColumn('sp_m_created', 'timestamp', [ 'timezone' => true, 'default' => 'CURRENT_TIMESTAMP' ])
            ->addIndex([ 'sp_m_id' ], [ 'unique' => true ])
            ->addIndex([ 'sp_m_account_id' ])
            ->create();

        $table = $this->table('sp_emails', [ 'id' => 'sp_e_id' ]);
        $table->addColumn('sp_e_title', 'string', [ 'limit' => 200 ])
            ->addColumn('sp_e_from_name', 'string', [ 'limit' => 200, 'default' => 'Hotlight' ])
            ->addColumn('sp_e_from_email', 'string', [ 'limit' => 200, 'default' => 'noreply@hotlight.com' ])
            ->addColumn('sp_e_message', 'text', [ 'limit' => 200 ])
            ->addColumn('sp_e_created', 'timestamp', [ 'timezone' => true, 'default' => 'CURRENT_TIMESTAMP' ])
            ->addIndex([ 'sp_e_id' ], [ 'unique' => true ])
            ->create();
    }
}
