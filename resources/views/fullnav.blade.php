<div class="topbar-nav header navbar" role="banner">
    <nav id="topbar">
        <ul class="navbar-nav theme-brand flex-row  text-center">
            <li class="nav-item theme-logo">
                <a href="<?= URL::to('/'); ?>">
                    <img src="/assets/img/90x90.jpg" class="navbar-logo" alt="logo">
                </a>
            </li>
            <li class="nav-item theme-text">
                <a href="<?= URL::to('/'); ?>" class="nav-link"> Spotlight </a>
            </li>
        </ul>

        <ul class="list-unstyled menu-categories" id="topAccordion">

            <li class="menu single-menu">
                <a href="<?= URL::to('/account'); ?>" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle autodroprown">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                        <span>Account</span>
                    </div>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg>
                </a>
                <ul class="collapse submenu list-unstyled" id="dashboard" data-parent="#topAccordion">
                    <li>
                        <a href="<?= URL::to('account'); ?>"> Edit </a>
                    </li>
                </ul>
            </li>

            <li class="menu single-menu">
                <a href="<?= URL::to('/account'); ?>" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle autodroprown">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                        <span>Payment Methods</span>
                    </div>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg>
                </a>
                <ul class="collapse submenu list-unstyled" id="dashboard" data-parent="#topAccordion">
                    <li>
                        <a href="<?= URL::to('cards/add'); ?>"> Add Card </a>
                    </li>
                    <li>
                        <a href="<?= URL::to('cards/manage'); ?>"> Manage Cards </a>
                    </li>
                    <li>
                        <a href="<?= URL::to('banks/add'); ?>"> Add Bank </a>
                    </li>
                    <li>
                        <a href="<?= URL::to('banks/manage'); ?>"> Manage Banks </a>
                    </li>
                </ul>
            </li>

            <li class="menu single-menu">
                <a href="#" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"  preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><g fill="none" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M23 7l-7 5l7 5V7z"/><rect x="1" y="5" width="15" height="14" rx="2" ry="2"/></g></svg>
                        <span>My Videos</span>
                    </div>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg>
                </a>
                <ul class="collapse submenu list-unstyled" id="app" data-parent="#topAccordion">
                    <li>
                        <a href="<?= URL::to('video/create'); ?>"> Create </a>
                    </li>
                    <li>
                        <a href="<?= URL::to('calendar'); ?>"> Calendar </a>
                    </li>
                    <li>
                        <a href="<?= URL::to('video'); ?>"> Created Videos </a>
                    </li>
                    <li>
                        <a href="<?= URL::to('video/purchased'); ?>"> Purcahsed Videos </a>
                    </li>
                </ul>
            </li>

            <li class="menu single-menu">
                <a href="#" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <div class="">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"  preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><g fill="none" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M23 7l-7 5l7 5V7z"/><rect x="1" y="5" width="15" height="14" rx="2" ry="2"/></g></svg>
                        <span>Search Videos/Hosts</span>
                    </div>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg>
                </a>
                <ul class="collapse submenu list-unstyled" id="components" data-parent="#topAccordion">
                    <li>
                        <a href="<?= URL::to('videos'); ?>"> Search Videos/Streams </a>
                    </li>
                    <li>
                        <a href="<?= URL::to('hosts'); ?>"> Search Hosts</a>
                    </li>
                </ul>
            </li>

        </ul>
    </nav>
</div>