@extends('account.notuser')

@section('title')
    Free Account Registration
@endsection

@section('content')
    <div id="content" class="main-content">
        <div class="layout-px-spacing">

            <div class="account-settings-container layout-top-spacing">

                <div class="account-content">
                    <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll" data-offset="-100">

                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                <form id="general-info" class="section general-info" method="post" action="account/submit" enctype="multipart/form-data">
                                    <div class="info">
                                        <h6 class="">Free Account Registration</h6>
                                        <div class="row">
                                            <div class="col-lg-11 mx-auto">
                                                <div class="row">
                                                    <div class="col-xl-2 col-lg-12 col-md-4">
                                                        <div class="upload mt-4 pr-md-4">
                                                            <input type="file" name="profile_img" id="input-file-max-fs" class="dropify" data-default-file="assets/img/200x200.jpg" data-max-file-size="2M" />
                                                            <p class="mt-2"><i class="flaticon-cloud-upload mr-1"></i> Upload Picture</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-10 col-lg-12 col-md-8 mt-md-0 mt-4">
                                                        <div class="form">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="firstName">First Name</label>
                                                                        <input type="text" class="form-control mb-4" name="first_name" id="first_name" placeholder="First Name" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="lastName">Last Name</label>
                                                                        <input type="text" class="form-control mb-4" name="last_name" id="last_name" placeholder="Last Name" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="username">Username (min 8) <span id="username-alert"></span></label>
                                                                        <input type="text" data-field="username" class="check-input form-control mb-4" name="username" id="username" placeholder="Username" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="email">Email <span id="email-alert"></span></label>
                                                                        <input type="text" data-field="email" class="check-input form-control mb-4" name="email" id="email" placeholder="Email" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="phone">Phone</label>
                                                                        <input type="text" class="form-control mb-4" name="phone" id="phone" placeholder="+1 234 567 8900" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="time_zome">Time Zone</label>
                                                                        <select class="form-control" id="time_zome" name="time_zome">
                                                                            @foreach($time_zones as $kTZ => $time_zone)
                                                                                <option value="{{ $time_zone[ key($time_zone) ] }}" @if($time_zone[ key($time_zone) ] == $account->sp_a_timezone) selected @endif >{{ $time_zone[ key($time_zone) ] }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                @include('account.geo_search')

                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="currency">Currency</label>
                                                                        <select class="form-control" id="currency" name="currency">
                                                                            @foreach($currencies['currency'] as $kC => $currency)
                                                                                <option value="{{ $kC }}" @if($kC == $account->sp_a_currency) selected @endif >{{ $kC.' - '.$currency }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="type">Account Type</label>
                                                                        <select class="form-control" id="type" name="type">
                                                                            <option value="1">Viewer</option>
                                                                            <option value="2">Host</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="description">Account Description</label>
                                                                <input type="text" class="form-control mb-4" name="description" id="description" placeholder="Please describe the purpose of your account." value="">
                                                            </div>
                                                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="account-settings-footer">

                    <div class="as-footer-container">

                        <div class="blockui-growl-message">
                            <i class="flaticon-double-check"></i>&nbsp; Settings Saved Successfully
                        </div>
                        <button id="multiple-messages" class="btn btn-primary">Create Free Account</button>

                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection