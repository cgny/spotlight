@extends('master')

@section('title')
    Add Debit/Credit Card
@endsection

@section('content')

    <!--  BEGIN TOPBAR  -->

    @yield('fullnav', View::make('fullnav'))

    <!--  END TOPBAR  -->

    <style>

        #clockBack {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0.7);
            z-index: 998;
            display: none;
        }

        #showLoadingClock {
            position: fixed;
            top: 50%;
            left: 50%;
            width: 50px;
            margin: 0 0 0 -25px;
            background: white;
            border-radius: 10px;
            text-align: center;
            padding: 10px;
            box-shadow: 2px 2px 2px #333;
            z-index: 999;
        }

        #loadIMGdiv {
            width: 100%;
            height: 100%;
            text-align: center;
            vertical-align: middle;
        }
    </style>


    <div id="clockBack">
        <div id="showLoadingClock">
            <img src="/assets/img/loadClock.gif" alt=""/>
        </div>
    </div>


    <div id="content" class="main-content">
        <div class="layout-px-spacing">

            <div class="account-settings-container layout-top-spacing">

                <div class="account-content">
                    <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll"
                         data-offset="-100">

                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                <form id="add_card" class="section general-info" method="post"
                                      action="account/submitBilling">
                                    <div class="info">
                                        <h6 class="">Add Debit/Credit Card</h6>
                                        <div class="row">
                                            <div class="col-lg-12 mx-auto">
                                                <div class="row">
                                                    <div class="col-xl-9 col-lg-9 col-md-8 mt-md-0 mt-4">
                                                        <div class="form">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="name_on_card">Saved Name of Card</label>
                                                                        <input type="text" class="form-control mb-4"
                                                                               maxlength="" name="saved_name"
                                                                               id="saved_name"
                                                                               placeholder="Favorite Card" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="name_on_card">Name on Card</label>
                                                                        <input type="text" class="form-control mb-4"
                                                                               maxlength="150" name="name_on_card"
                                                                               id="name_on_card"
                                                                               placeholder="Name On Card" value="">
                                                                    </div>
                                                                </div>


                                                                <div class="col-sm-6">
                                                                    <label for="card-element">Credit or Debit Card</label>
                                                                    <div class="form-group">
                                                                        <div id="card-element" class="form-control" >
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="phone">Set as Default</label>
                                                                            <div class="n-chk">
                                                                                <label class="new-control new-checkbox checkbox-primary">
                                                                                    <input type="checkbox" id="s_default" name="s_default" class="new-control-input" value="1" />
                                                                                    <span class="new-control-indicator"></span>Yes
                                                                                </label>
                                                                            </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <h6 class="">Billing Address</h6>
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="phone">Same as home address</label>
                                                                            <div class="n-chk">
                                                                                <label class="new-control new-checkbox checkbox-primary">
                                                                                    <input type="checkbox" id="same_home" name="same_home" class="new-control-input" value="1" />
                                                                                    <span class="new-control-indicator"></span>Yes
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="card_cvc">Address</label>
                                                                        <input type="text" class="form-control mb-4"
                                                                               maxlength="150" name="address_1"
                                                                               id="address_1" placeholder="" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="postal">Address (optional)</label>
                                                                        <input type="text" class="form-control mb-4"
                                                                               maxlength="150" name="address_2"
                                                                               id="address_2" placeholder="" value="">
                                                                    </div>
                                                                </div>

                                                                @include('account.geo_search')

                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="postal">Zip/Postal Code</label>
                                                                        <input type="text" class="form-control mb-4"
                                                                               maxlength="10" name="postal" id="postal"
                                                                               placeholder="" value="">
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="postal">Phone</label>
                                                                        <input type="text" class="form-control mb-4"
                                                                               maxlength="13" name="phone" id="phone"
                                                                               placeholder="" value="">
                                                                    </div>
                                                                </div>

                                                                <input type="hidden" name="_token" id="token"
                                                                       value="{{ csrf_token() }}">
                                                                <button id="do_addCard" class="btn btn-primary" onClick="">Save Debit Card</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="account-settings-footer">

                    <div class="as-footer-container">
                        &nbsp;&nbsp;
                        <div class="blockui-growl-message">
                            <i class="flaticon-double-check"></i>&nbsp;
                        </div>
                        <span class="" id="add_card_form_errors"></span>
                        <div id="card-errors" role="alert"></div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection

@section('code_footer')

    <script src="/assets/plugins/highlight/highlight.pack.js"></script>
    <script src="/assets/plugins/input-mask/jquery.inputmask.bundle.min.js"></script>
    <script src="/assets/plugins/input-mask/input-mask.js"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <script type="text/javascript">
        //Stripe.setPublishableKey('pk_live_4hFoQqbbxgCf3WKDngKUj7mv00oF3HgGYC');
        var stripe = Stripe('pk_test_SJz5X8V0QmIbvmeKqLePHkiL000duwiXdK');
        var elements = stripe.elements();
        var style = {
            base: {
                color: "#32325d",
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: "antialiased",
                fontSize: "16px",
                "::placeholder": {
                    color: "#aab7c4"
                }
            },
            invalid: {
                color: "#fa755a",
                iconColor: "#fa755a"
            }
        };
        var cardElement = elements.create('card', {style : style });
        cardElement.mount('#card-element');
    </script>
    <script>

        var ua = navigator.userAgent,
            touchClick = (ua.match(/iPad/i)) ? "touchstart" : "click";
        var host = 'http://local.spotlight.com';
        var baseURL = host + '/';

        var address = {
            address: '{{ $account->sp_a_address_1 }}',
            address2: '{{ $account->sp_a_address_2 }}',
            city: '{{ $account->sp_a_city }}',
            state: '{{ $account->sp_a_state }}',
            postal: '{{ $account->sp_a_postal_code }}',
            phone: '{{ $account->sp_a_phone }}',
        };

        $('#same_home').click(function()
        {
            if($(this).prop('checked') == true)
            {
                $('#address_1').val( address.address );
                $('#address_2').val( address.address2 );
                $('#city').val( address.city );
                $('#state').val( address.state );
                $('#postal').val( address.postal );
                $('#phone').val( address.phone );
            }
            else
            {
                $('#address_1').val( '' );
                $('#address_2').val( '' );
                $('#city').val( '' );
                $('#state').val( '' );
                $('#postal').val( '' );
                $('#phone').val( '' );
            }
        });

        cardElement.on('change', function(event)
        {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        var form = document.getElementById('add_card');

        form.addEventListener('submit', function(ev)
        {
            ev.preventDefault();
            $('#clockBack').fadeIn();
            stripe.confirmCardSetup('{{ $intent->client_secret }}',{
                payment_method: {
                    card: cardElement,
                    billing_details: {
                        address: {
                            line1: $('#address_1').val(),
                            line2: $('#address_2').val(),
                            city: $('option:selected', $('#city')).attr('data-code'),
                            state: $('#state').val(),
                            postal_code: $('#postal').val()
                        },
                        name: $('#name_on_card').val(),
                        phone: $('#phone').val(),
                    }
                }
            }).then(function(result) {
                console.log(result);
                if (result.error) {
                    // Show error to your customer (e.g., insufficient funds)
                    $('#clockBack').fadeOut();
                } else {
                    // The payment has been processed!
                    stripeResponseHandler(result);
                }
            });
        });

        function stripeResponseHandler(response)
        {
            if (response.error)
            {
                // show the errors on the form
                $("#add_card_form_errors").text(response.error);
                $('#clockBack').fadeOut();
            }
            else
            {
                $("#add_card_form_errors").text('Adding...');
                var _token = $('#token').val();
                var postData = {
                    si_id: response.setupIntent.id,
                    pm_id: response.setupIntent.payment_method,
                    saved_name: $('#saved_name').val(),
                    s_default: $('#s_default').val(),
                    _token: _token,
                };

                $.post(baseURL + 'cards/submitCard', postData, function (data)
                {
                    $('#clockBack').fadeOut();
                    if (data.success == true) {
                        $("#add_card_form_errors").html('Added Successfuly!').css({'color': 'green'});
                        $('do_addCard').removeClass('btn-primary').addClass('btn btn-success').text('Reload').attr('id', 'refresh');
                    }
                    else
                    {
                        $("#add_card_form_errors").text(data.error.message).css({'color': 'red'});
                    }
                });
            }
        }

        $('body').delegate('#refresh', touchClick, function ()
        {
            window.location.reload();
        });

        $('#showAddForm').click(function ()
        {
            $('#add_card').slideDown();
        });


    </script>
@endsection