@extends('master')

@section('title')
    Manage Banks
@endsection

@section('content')

    <!--  BEGIN TOPBAR  -->

    @yield('fullnav', View::make('fullnav'))

    <!--  END TOPBAR  -->

    <style>

        #clockBack {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0.7);
            z-index: 998;
            display: none;
        }

        #showLoadingClock {
            position: fixed;
            top: 50%;
            left: 50%;
            width: 50px;
            margin: 0 0 0 -25px;
            background: white;
            border-radius: 10px;
            text-align: center;
            padding: 10px;
            box-shadow: 2px 2px 2px #333;
            z-index: 999;
        }

        #loadIMGdiv {
            width: 100%;
            height: 100%;
            text-align: center;
            vertical-align: middle;
        }
    </style>


    <div id="clockBack">
        <div id="showLoadingClock">
            <img <img src="/assets/img/loadClock.gif" alt=""/>
        </div>
    </div>


    <div id="content" class="main-content">
        <div class="layout-px-spacing">

            <div class="account-settings-container layout-top-spacing">

                <div class="account-content">
                    <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll"
                         data-offset="-100">

                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                <form id="add_bank" class="section general-info" method="post"
                                      action="submitBank">
                                    <div class="info">
                                        <div class="row">
                                            <div class="col-lg-12 mx-auto">
                                                <div class="row">
                                                    <div id="bank-list" class="col-lg-12 mx-auto">
                                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="width:100%" height="5em" style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 512 512"><path fill="#626262" d="M247.759 14.358L16 125.946V184h480v-58.362zM464 152H48v-5.946l200.241-96.412L464 146.362z"/><path fill="#626262" d="M48 408h416v32H48z"/><path fill="#626262" d="M16 464h480v32H16z"/><path fill="#626262" d="M56 216h32v160H56z"/><path fill="#626262" d="M424 216h32v160h-32z"/><path fill="#626262" d="M328 216h32v160h-32z"/><path fill="#626262" d="M152 216h32v160h-32z"/><path fill="#626262" d="M240 216h32v160h-32z"/></svg>
                                                        <h6 style="text-align: center">Manage Bank Accounts</h6>
                                                        <div id="banks">
                                                        @foreach($banks as $bank)
                                                            <div class="col-lg-6 mx-auto" style="float: left;">
                                                                    <div class="card component-card_2" style="margin-top:20px;text-align: center;padding-top:10px"
                                                                     data-categories="">
                                                                       <div class="card-body" style="">
                                                                           <div class="col-lg-6 pull-left" style="float: left;">
                                                                                <h5 class="card-title">{{ $bank->sp_b_name }}</h5>
                                                                                <p class="card-text">{{ $bank->sp_b_bank }}</p>
                                                                                <p class="card-text">{{ $bank->sp_b_account_number_4 }}</p>
                                                                                <p class="card-text">{{ $bank->sp_b_currency }}</p>
                                                                           </div>
                                                                           <div class="col-lg-6 pull-left" style="float: left;">
                                                                                  <a href="{{ URL::to('banks/view/' . substr($bank->sp_b_stripe_id,3) ) }}">
                                                                                      <button class="btn btn-primary edit-bank" data-id="'+ data[ type ][x].sp_b_name +'">Edit</button>
                                                                                   </a>
                                                                               <button class="btn btn-primary set-remove" data-type="banks" data-id="{{ substr($bank->sp_b_stripe_id,3) }}" onclick="return false">Remove</button><br><br>
                                                                               @if( $bank->sp_b_default == 0 )
                                                                                   <button class="btn btn-primary set-default" data-type="banks" data-id="{{ substr($bank->sp_b_stripe_id,3) }}" data-id="{{ $bank->sp_b_name }}" onclick="return false">Set as Default</button>
                                                                               @else
                                                                                   <b>Default</b>
                                                                               @endif
                                                                           </div>
                                                                        </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="account-settings-footer">

                    <div class="as-footer-container">
                        &nbsp;&nbsp;
                        <div class="blockui-growl-message">
                            <i class="flaticon-double-check"></i>&nbsp;
                        </div>

                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection

@section('code_footer')

    <script>

        $(function ()
        {

        });

    </script>

@endsection