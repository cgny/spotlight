@extends('master')

@section('title')
    Account Debit Card
@endsection

@section('content')

    <!--  BEGIN TOPBAR  -->

    @yield('fullnav', View::make('fullnav'))

    <!--  END TOPBAR  -->

    <style>

        #clockBack {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0.7);
            z-index: 998;
            display: none;
        }

        #showLoadingClock {
            position: fixed;
            top: 50%;
            left: 50%;
            width: 50px;
            margin: 0 0 0 -25px;
            background: white;
            border-radius: 10px;
            text-align: center;
            padding: 10px;
            box-shadow: 2px 2px 2px #333;
            z-index: 999;
        }

        #loadIMGdiv {
            width: 100%;
            height: 100%;
            text-align: center;
            vertical-align: middle;
        }
    </style>


    <div id="clockBack">
        <div id="showLoadingClock">
            <img <img src="/assets/img/loadClock.gif" alt=""/>
        </div>
    </div>


    <div id="content" class="main-content">
        <div class="layout-px-spacing">

            <div class="account-settings-container layout-top-spacing">

                <div class="account-content">
                    <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll"
                         data-offset="-100">

                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                <form id="add_bank" class="section general-info" method="post"
                                      action="submitBank">
                                    <div class="info">
                                        <h6 class="">Add Bank Account</h6>
                                        <div class="row">
                                            <div class="col-lg-12 mx-auto">
                                                <div class="row">
                                                    <div class="col-xl-9 col-lg-9 col-md-8 mt-md-0 mt-4">
                                                        <div class="form">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="name_on_card">Saved Name</label>
                                                                        <input type="text" class="form-control mb-4"
                                                                               maxlength="" name="saved_name"
                                                                               id="saved_name"
                                                                               placeholder="" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="card_number">Bank Name</label>
                                                                        <input type="text" class="form-control mb-4"
                                                                               maxlength="" name="a_bank"
                                                                               id="a_bank"
                                                                               placeholder="" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="card_exp_month">Account Holder Name
                                                                            (optional)</label>
                                                                        <input type="text" class="form-control mb-4"
                                                                               name="a_holder" id="a_holder"
                                                                               placeholder="" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="card_exp_year">Account Holder Type
                                                                            (optional)</label>
                                                                        <select class="form-control" id="b_type"
                                                                                name="b_type">
                                                                            <option value="individual">Individual
                                                                            </option>
                                                                            <option value="company">Company</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="card_exp_month">Account Number
                                                                            (Checking accounts only)</label>
                                                                        <input type="number" class="form-control mb-4"
                                                                               name="a_number" id="a_number"
                                                                               placeholder="" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="card_exp_month">Routing Number
                                                                            (optional, US required)</label>
                                                                        <input type="number" class="form-control mb-4"
                                                                               name="r_number" id="r_number"
                                                                               placeholder="" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="currency">Currency</label>
                                                                        <select class="form-control" id="currency"
                                                                                name="currency">
                                                                            @foreach($currencies['currency'] as $kC => $currency)
                                                                                <option value="{{ $kC }}"
                                                                                        @if($kC == $account->sp_a_currency) selected @endif >{{ $kC.' - '.$currency }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                @include('account.geo_search')

                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="postal">Zip/Postal Code</label>
                                                                        <input type="text" class="form-control mb-4"
                                                                               maxlength="10" name="postal" id="postal"
                                                                               placeholder="" value="">
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="ssn_last_4">SSN last 4 (US only
                                                                            required)</label>
                                                                        <input type="text" class="form-control mb-4"
                                                                               maxlength="4" name="ssn_last_4"
                                                                               id="ssn_last_4" placeholder="" value="">
                                                                    </div>
                                                                </div>
                                                                <input type="hidden" name="_token" id="token"
                                                                       value="{{ csrf_token() }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="account-settings-footer">

                    <div class="as-footer-container">
                        &nbsp;&nbsp;
                        <div class="blockui-growl-message">
                            <i class="flaticon-double-check"></i>&nbsp;
                        </div>
                        <button id="do_addBank" class="btn btn-primary" onsubmit="return false;">Save Bank</button>
                        <span class="" id="add_card_form_errors"></span>

                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection

@section('code_footer')

    <script src="/assets/plugins/highlight/highlight.pack.js"></script>
    <script src="/assets/plugins/input-mask/jquery.inputmask.bundle.min.js"></script>
    <script src="/assets/plugins/input-mask/input-mask.js"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <script type="text/javascript">
        //Stripe.setPublishableKey('pk_live_4hFoQqbbxgCf3WKDngKUj7mv00oF3HgGYC');
        var stripe = Stripe('pk_test_SJz5X8V0QmIbvmeKqLePHkiL000duwiXdK');
    </script>
    <script>
        $(function () {

            var ua = navigator.userAgent,
                touchClick = (ua.match(/iPad/i)) ? "touchstart" : "click";
            var host = 'http://local.spotlight.com';
            var baseURL = host + '/';

            var address = {
                address: '{{ $account->sp_a_address_1 }}',
                address2: '{{ $account->sp_a_address_2 }}',
                city: '{{ $account->sp_a_city }}',
                state: '{{ $account->sp_a_state }}',
                postal: '{{ $account->sp_a_postal_code }}'
            };

            function stripeResponseHandler(response) {
                if (response.error) {
                    // show the errors on the form
                    $("#add_card_form_errors").text(response.error.message);
                    $('#clockBack').fadeOut();
                }
                else {
                    $("#add_card_form_errors").text('Adding...');
                    // token contains id, last4, and card type
                    var token = response.token.id;
                    // insert the token into the form so it gets submitted to the server

                    var _token = $('#token').val();

                    var postData = {
                        r_number: $('#r_number').val().substr(-4),
                        a_number: $('#a_number').val().substr(-4),
                        a_bank: $('#a_bank').val(),
                        saved_name: $('#saved_name').val(),
                        currency: $('#currency').val(),
                        country: $('#country').val(),
                        stripeToken: token,
                        account_id: '{{ $account->sp_a_id }}',
                        _token: _token
                    };

                    $('#clockBack').fadeIn();

                    $.post(baseURL + 'banks/submitBank', postData, function (data) {
                        $('#clockBack').fadeOut();
                        $('#stripeToken').remove();
                        if (data.error.sucess == true) {
                            $("#add_card_form_errors").html('Added Successfuly!').css({'color': 'green'});
                            $('do_addCard').removeClass('btn-primary').addClass('btn btn-success').text('Reload').attr('id', 'refresh');
                        }
                        else {
                            $("#add_card_form_errors").text(data.error.message + ' ' + data.acct_update.error).css({'color': 'red'});
                        }
                    });
                }
            }

            $(function () {

                $('body').delegate('#do_addBank', touchClick, function () {
                    stripe
                        .createToken('bank_account', {
                            country: $('#country').val(),
                            currency: $('#currency').val(),
                            routing_number: $('#r_number').val(),
                            account_number: $('#a_number').val(),
                            account_holder_name: $('#a_holder').val(),
                            account_holder_type: $('#type').val(),
                        })
                        .then(function (result) {
                            stripeResponseHandler(result);
                        });

                });
            });

            $('body').delegate('#refresh', touchClick, function () {
                window.location.reload();
            });

            $('#showAddForm').click(function () {
                $('#add_bank').slideDown();
            });

        });
    </script>

@endsection