@extends('master')

@section('title')
    Account Details
@endsection

@section('content')

    <!--  BEGIN TOPBAR  -->

    @yield('fullnav', View::make('fullnav',['account' => $account]))

    <!--  END TOPBAR  -->

    <div id="content" class="main-content">
        <div class="layout-px-spacing">

            <div class="account-settings-container layout-top-spacing">

                <div class="account-content">
                    <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll" data-offset="-100">

                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                <form id="general-info" class="section general-info" method="post" action="account/update" enctype="multipart/form-data">
                                    <div class="info">
                                        <h6 class="">Account Information</h6>
                                        <div class="row">
                                            <div class="col-lg-11 mx-auto">
                                                <div class="row">
                                                    <div class="col-xl-2 col-lg-12 col-md-4">
                                                        <div class="upload mt-4 pr-md-4">
                                                            <input type="file" name="profile_img" id="input-file-max-fs" class="dropify" data-default-file="storage/{{ $account->sp_a_photo }}" data-max-file-size="5M" />
                                                            <p class="mt-2"><i class="flaticon-cloud-upload mr-1"></i> Upload Picture</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-10 col-lg-12 col-md-8 mt-md-0 mt-4">
                                                        <div class="form">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <h6 class="">General Information</h6>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="lastName">First Name</label>
                                                                        <input type="text" class="form-control mb-4" name="first_name" id="first_name" placeholder="First Name" value="{{ $account->sp_a_first_name }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="lastName">Last Name</label>
                                                                        <input type="text" class="form-control mb-4" name="last_name" id="last_name" placeholder="Last Name" value="{{ $account->sp_a_last_name }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="username">Username (min 8) <span id="username-alert"></span></label>
                                                                        <input type="text" data-field="username" class="check-input form-control mb-4" name="username" id="username" placeholder="Username" value="{{ $account->sp_a_username }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="business_name">Business Name</label>
                                                                        <input type="text" class="form-control mb-4" name="business_name" id="business_name" placeholder="DBA (Business Name)" value="{{ $account->sp_a_business_name }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="email">Email  <span id="email-alert"></span></label>
                                                                        <input type="text" data-field="email" class="check-input form-control mb-4" name="email" id="email" placeholder="Email" value="{{ $account->sp_a_email }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="phone">Phone</label>
                                                                        <input type="text" class="form-control mb-4" name="phone" id="phone" placeholder="+1 234 567 8900" value="{{ $account->sp_a_phone }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="phone">DOB Month</label>
                                                                        <input type="text" class="form-control mb-4" name="dob_m" id="dob_m" placeholder="" value="{{ $account->sp_a_dob_m }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="phone">DOB Day</label>
                                                                        <input type="text" class="form-control mb-4" name="dob_d" id="dob_d" placeholder="" value="{{ $account->sp_a_dob_d }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <div class="form-group">
                                                                        <label for="phone">Dob Year</label>
                                                                        <input type="text" class="form-control mb-4" name="dob_y" id="dob_y" placeholder="" value="{{ $account->sp_a_dob_y }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <h6 class="">Home Address</h6>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="card_cvc">Address</label>
                                                                        <input type="text" class="form-control mb-4"
                                                                               maxlength="" name="address_1"
                                                                               id="address_1" placeholder="" value="{{ $account->sp_a_address_1 }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="postal">Address (optional)</label>
                                                                        <input type="text" class="form-control mb-4"
                                                                               maxlength="" name="address_2"
                                                                               id="address_2" placeholder="" value="{{ $account->sp_a_address_2 }}">
                                                                    </div>
                                                                </div>

                                                                @include('account.geo_search')

                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="postal">Zip/Postal Code</label>
                                                                        <input type="text" class="form-control mb-4"
                                                                               maxlength="10" name="postal" id="postal"
                                                                               placeholder="" value="{{ $account->sp_a_postal_code }}">
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="country">Product/Services offered Category</label>
                                                                        <select class="form-control" id="mcc"
                                                                                name="mcc">
                                                                            @foreach($mccs as $code => $mcc)
                                                                                <option value="{{ $code }}"
                                                                                        @if($code == $account->sp_a_industry) selected @endif >{{ $mcc }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <h6 class="">Spotlight Details</h6>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="type">Account Type</label>
                                                                        <select class="form-control" id="type" name="type">
                                                                            <option value="1">Viewer</option>
                                                                            <option value="2">Host</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="time_zome">Time Zone</label>
                                                                        <select class="form-control" id="time_zome" name="time_zome">
                                                                            @foreach($time_zones as $kTZ => $time_zone)
                                                                                <option value="{{ $time_zone[ key($time_zone) ] }}" @if($time_zone[ key($time_zone) ] == $account->sp_a_timezone) selected @endif >{{ $time_zone[ key($time_zone) ] }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="description">Account Description</label>
                                                                        <input type="text" class="form-control mb-4" name="description" id="description" placeholder="Please describe the purpose of your account." value="{{ $account->sp_a_description }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="phone">Categories</label>
                                                                        @foreach($categories as $category)
                                                                            <div class="n-chk">
                                                                                <label class="new-control new-checkbox checkbox-primary">
                                                                                    <input type="checkbox" name="categories[]" class="new-control-input" value="{{ $category->sp_c_id }}" @if( in_array($category->sp_c_id,$account_categories) ) checked @endif />
                                                                                    <span class="new-control-indicator"></span>{{ $category->sp_c_name }}
                                                                                </label>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="account-settings-footer">

                    <div class="as-footer-container">

                        <button id="multiple-reset" class="btn btn-warning">Reset All</button>
                        <div class="blockui-growl-message">
                            <i class="flaticon-double-check"></i>&nbsp; Settings Saved Successfully
                        </div>
                        <button id="multiple-messages" class="btn btn-primary">Save Changes</button>

                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection

@section('code_footer')
    <script src="/assets/plugins/input-mask/jquery.inputmask.bundle.min.js"></script>
    <script src="/assets/plugins/input-mask/input-mask.js"></script>
    <script>
        ac_username = '{{ $account->sp_a_username }}';
        ac_email = '{{ $account->sp_a_email }}';
        jQuery(function()
        {

            jQuery('#type').val('{{ $account->sp_a_account_type }}');


            $('#ssn_last_4').inputmask({mask: "9999"});
            $('#dob_m').inputmask({mask: "99"});
            $('#dob_d').inputmask({mask: "99"});
            $('#dob_y').inputmask({mask: "9999"});

        });
    </script>
@endsection