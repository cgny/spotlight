

<div class="col-sm-6">
    <div class="form-group">
        <label for="country">Country</label>
        <select class="form-control choose-country" id="country" name="country">
            @foreach($countries as $country)
                <option data-code="{{ $country->id }}" value="{{ $country->iso2 }}" @if($country->iso2 == $account->sp_a_country) selected @endif >{{ $country->iso2.' - '.$country->name.' '.$country->emoji }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="col-sm-6">
    <div class="form-group">
        <label for="state">State</label>
        <select class="form-control choose-state" id="state" name="state">
            @foreach($states as $state)
                <option data-code="{{ $state->id }}" value="{{ $state->iso2 }}" @if($state->iso2 == $account->sp_a_state) selected @endif >{{ $state->iso2.' - '.$state->name }}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="col-sm-6">
    <div class="form-group">
        <label for="city">City</label>
        <select class="form-control" id="city" name="city">
            @foreach($cities as $city)
                <option data-code="{{ $city->id }}" value="{{ $city->id }}" data-name="{{ $city->name }}" @if($city->id == $account->sp_a_city) selected @endif >{{ $city->name }}</option>
            @endforeach
        </select>
    </div>
</div>
