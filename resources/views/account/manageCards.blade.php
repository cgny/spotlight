@extends('master')

@section('title')
    Manage Debit Cards
@endsection

@section('content')

    <!--  BEGIN TOPBAR  -->

    @yield('fullnav', View::make('fullnav'))

    <!--  END TOPBAR  -->

    <style>

        #clockBack {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0.7);
            z-index: 998;
            display: none;
        }

        #showLoadingClock {
            position: fixed;
            top: 50%;
            left: 50%;
            width: 50px;
            margin: 0 0 0 -25px;
            background: white;
            border-radius: 10px;
            text-align: center;
            padding: 10px;
            box-shadow: 2px 2px 2px #333;
            z-index: 999;
        }

        #loadIMGdiv {
            width: 100%;
            height: 100%;
            text-align: center;
            vertical-align: middle;
        }
    </style>


    <div id="clockBack">
        <div id="showLoadingClock">
            <img <img src="/assets/img/loadClock.gif" alt=""/>
        </div>
    </div>


    <div id="content" class="main-content">
        <div class="layout-px-spacing">

            <div class="account-settings-container layout-top-spacing">

                <div class="account-content">
                    <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll"
                         data-offset="-100">

                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                <form id="add_card" class="section general-info" method="post"
                                      action="account/submitBilling">
                                    <div class="info">
                                        <div class="row">
                                            <div class="col-lg-12 mx-auto">
                                                <div class="row">
                                                    <div id="card-list" class="col-lg-12 mx-auto">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                                             aria-hidden="true" focusable="false" width="100%"
                                                             height="5em"
                                                             style="-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); transform: rotate(360deg);"
                                                             preserveAspectRatio="xMidYMid meet"
                                                             viewBox="0 0 16 16">
                                                            <g fill="#626262">
                                                                <path fill-rule="evenodd"
                                                                      d="M14 3H2a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4a1 1 0 0 0-1-1zM2 2a2 2 0 0 0-2 2v8a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V4a2 2 0 0 0-2-2H2z"/>
                                                                <rect width="3" height="3" x="2" y="9" rx="1"/>
                                                                <path d="M1 5h14v2H1z"/>
                                                            </g>
                                                        </svg>
                                                        <h6 style="text-align: center">Manage Credit/Debit Cards</h6>
                                                        <div id="cards">
                                                        @foreach($cards as $card)
                                                                <div class="col-lg-6 mx-auto" style="float: left;">
                                                                    <div class="card component-card_2" style="margin-top:20px;text-align: center;padding-top:10px"
                                                                     data-categories="">
                                                                        <div class="card-body">
                                                                            <h5 class="card-title">{{ $card->sp_ca_name }}</h5>
                                                                            <p class="card-text"><b>{{ $card->sp_ca_type }}</b> {{ $card->sp_ca_last_4 }}</p>
                                                                            <p class="card-text">{{ $card->sp_ca_month }} / {{ $card->sp_ca_year }}</p>
                                                                            <button class="btn btn-primary set-remove" ddata-id="{{ substr($card->sp_ca_stripe_id,5) }}" onclick="return false">Remove</button><br><br>
                                                                            @if( $card->sp_ca_default == 0 )
                                                                                <button class="btn btn-primary set-default" data-type="cards" data-id="{{ substr($card->sp_ca_stripe_id,3) }}" data-id="{{ $card->sp_ca_name }}" onclick="return false">Set as Default</button>
                                                                            @else
                                                                                <b>Default</b>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                        @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="account-settings-footer">

                    <div class="as-footer-container">
                        &nbsp;&nbsp;
                        <div class="blockui-growl-message">
                            <i class="flaticon-double-check"></i>&nbsp;
                        </div>
                        <span class="" id="add_card_form_errors"></span>

                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection

@section('code_footer')

    <script>

        $(function ()
        {

        });

    </script>

@endsection