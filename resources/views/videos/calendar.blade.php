@extends('master',['account' => $account])

@section('title')
    Account Details
@endsection

@section('css')
    <link href="/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
    <link href="/plugins/fullcalendar/custom-fullcalendar.advance.css" rel="stylesheet" type="text/css" />
    <link href="/plugins/flatpickr/flatpickr.css" rel="stylesheet" type="text/css">
    <link href="/plugins/flatpickr/custom-flatpickr.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/forms/theme-checkbox-radio.css" rel="stylesheet" type="text/css" />
    <style>
        .widget { margin-bottom: 10px; }
        .widget-content-area { border-radius: 6px; }
        .daterangepicker.dropdown-menu {
            z-index: 1059;
        }
    </style>
@endsection

@section('content')

    <!--  BEGIN TOPBAR  -->

    @yield('fullnav', View::make('fullnav',['account' => $account]))


        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">
                <div class="row layout-top-spacing" id="cancel-row">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="statbox widget box box-shadow">
                            <div class="widget-content widget-content-area">
                                <div class="calendar-upper-section">
                                    <div class="row">
                                        <div class="col-md-8 col-12">
                                            <div class="labels">
                                                <p class="label label-primary">Purchased ({{ count($purchased_videos ) }})</p>
                                                <p class="label label-success">Created ({{ count($created_videos) }})</p>
                                            </div>
                                        </div>                                                
                                        <div class="col-md-4 col-12">
                                            <form action="javascript:void(0);" class="form-horizontal mt-md-0 mt-3 text-md-right text-center">
                                                <button id="myBtn" class="btn btn-primary"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar mr-2"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg> Add Event</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div id="calendar"></div>
                            </div>
                        </div>
                    </div>

                    <!-- The Modal -->
                    <div id="addEventsModal" class="modal animated fadeIn">

                        <div class="modal-dialog modal-dialog-centered">
                            
                            <!-- Modal content -->
                            <div class="modal-content">

                                <div class="modal-body">

                                    <span class="close">&times;</span>

                                    <div class="add-edit-event-box">
                                        <div class="add-edit-event-content">
                                            <h5 class="add-event-title modal-title">Add Events</h5>
                                            <h5 class="edit-event-title modal-title">Edit Events</h5>

                                            <form class="">

                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <label for="start-date" class="">Event Title:</label>
                                                        <div class="d-flex event-title">
                                                            <input id="write-e" type="text" placeholder="Enter Title" class="form-control" name="task">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 col-sm-6 col-12">
                                                        <div class="form-group start-date">
                                                            <label for="start-date" class="">From:</label>
                                                            <div class="d-flex">
                                                                <input id="start-date" placeholder="Start Date" class="form-control" type="text">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-12">
                                                        <div class="form-group end-date">
                                                            <label for="end-date" class="">To:</label>
                                                            <div class="d-flex">
                                                                <input id="end-date" placeholder="End Date" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label for="start-date" class="">Event Description:</label>
                                                        <div class="d-flex event-description">
                                                            <textarea id="taskdescription" placeholder="Enter Description" rows="3" class="form-control" name="taskdescription"></textarea>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="event-badge">
                                                            <p class="">Badge:</p>

                                                            <div class="d-sm-flex d-block">
                                                                <div class="n-chk">
                                                                    <label class="new-control new-radio radio-primary">
                                                                      <input type="radio" class="new-control-input" name="marker" value="bg-primary">
                                                                      <span class="new-control-indicator"></span>Work
                                                                    </label>
                                                                </div>

                                                                <div class="n-chk">
                                                                    <label class="new-control new-radio radio-warning">
                                                                      <input type="radio" class="new-control-input" name="marker" value="bg-warning">
                                                                      <span class="new-control-indicator"></span>Travel
                                                                    </label>
                                                                </div>

                                                                <div class="n-chk">
                                                                    <label class="new-control new-radio radio-success">
                                                                      <input type="radio" class="new-control-input" name="marker" value="bg-success">
                                                                      <span class="new-control-indicator"></span>Personal
                                                                    </label>
                                                                </div>

                                                                <div class="n-chk">
                                                                    <label class="new-control new-radio radio-danger">
                                                                      <input type="radio" class="new-control-input" name="marker" value="bg-danger">
                                                                      <span class="new-control-indicator"></span>Important
                                                                    </label>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>

                                </div>

                                <div class="modal-footer">
                                    <button id="discard" class="btn" data-dismiss="modal">Discard</button>
                                    <button id="add-e" class="btn">Add Task</button>
                                    <button id="edit-event" class="btn">Save</button>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>
    <!-- END MAIN CONTAINER -->
@endsection

@section('code_footer')

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="/plugins/fullcalendar/moment.min.js"></script>
    <script src="/plugins/flatpickr/flatpickr.js"></script>
    <script src="/plugins/fullcalendar/fullcalendar.min.js"></script>
    <!-- END PAGE LEVEL SCRIPTS -->

    <!--  BEGIN CUSTOM SCRIPTS FILE  -->
    <script>

            // Get the modal
            var modal = document.getElementById("addEventsModal");

            // Get the button that opens the modal
            var btn = document.getElementById("myBtn");

            // Get the Add Event button
            var addEvent = document.getElementById("add-e");
            // Get the Edit Event button
            var editEvent = document.getElementById("edit-event");
            // Get the Discard Modal button
            var discardModal = document.querySelectorAll("[data-dismiss='modal']")[0];

            // Get the Add Event button
            var addEventTitle = document.getElementsByClassName("add-event-title")[0];
            // Get the Edit Event button
            var editEventTitle = document.getElementsByClassName("edit-event-title")[0];

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            // Get the all <input> elements insdie the modal
            var input = document.querySelectorAll('input[type="text"]');
            var radioInput = document.querySelectorAll('input[type="radio"]');

            // Get the all <textarea> elements insdie the modal
            var textarea = document.getElementsByTagName('textarea');

            // Create BackDrop ( Overlay ) Element
            function createBackdropElement () {
                var btn = document.createElement("div");
                btn.setAttribute('class', 'modal-backdrop fade show')
                document.body.appendChild(btn);
            }

            // Reset radio buttons

            function clearRadioGroup(GroupName) {
                var ele = document.getElementsByName(GroupName);
                for(var i=0;i<ele.length;i++)
                    ele[i].checked = false;
            }

            // Reset Modal Data on when modal gets closed
            function modalResetData() {
                modal.style.display = "none";
                for (i = 0; i < input.length; i++) {
                    input[i].value = '';
                }
                for (j = 0; j < textarea.length; j++) {
                    textarea[j].value = '';
                    i
                }
                clearRadioGroup("marker");
                // Get Modal Backdrop
                var getModalBackdrop = document.getElementsByClassName('modal-backdrop')[0];
                document.body.removeChild(getModalBackdrop)
            }

            // When the user clicks on the button, open the modal
            btn.onclick = function() {
                modal.style.display = "block";
                addEvent.style.display = 'block';
                editEvent.style.display = 'none';
                addEventTitle.style.display = 'block';
                editEventTitle.style.display = 'none';
                document.getElementsByTagName('body')[0].style.overflow = 'hidden';
                createBackdropElement();
                enableDatePicker();
            }

            // Clear Data and close the modal when the user clicks on Discard button
            discardModal.onclick = function() {
                modalResetData();
                document.getElementsByTagName('body')[0].removeAttribute('style');
            }

            // Clear Data and close the modal when the user clicks on <span> (x).
            span.onclick = function() {
                modalResetData();
                document.getElementsByTagName('body')[0].removeAttribute('style');
            }

            // Clear Data and close the modal when the user clicks anywhere outside of the modal.
            window.onclick = function(event) {
                if (event.target == modal) {
                    modalResetData();
                    document.getElementsByTagName('body')[0].removeAttribute('style');
                }
            }

            newDate = new Date()
            monthArray = [ '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12' ]


            function getDynamicMonth( monthOrder ) {
                var getNumericMonth = parseInt(monthArray[newDate.getMonth()]);
                var getNumericMonthInc = parseInt(monthArray[newDate.getMonth()]) + 1;
                var getNumericMonthDec = parseInt(monthArray[newDate.getMonth()]) - 1;

                if (monthOrder === 'default') {

                    if (getNumericMonth < 10 ) {
                        return '0' + getNumericMonth;
                    } else if (getNumericMonth >= 10) {
                        return getNumericMonth;
                    }

                } else if (monthOrder === 'inc') {

                    if (getNumericMonthInc < 10 ) {
                        return '0' + getNumericMonthInc;
                    } else if (getNumericMonthInc >= 10) {
                        return getNumericMonthInc;
                    }

                } else if (monthOrder === 'dec') {

                    if (getNumericMonthDec < 10 ) {
                        return '0' + getNumericMonthDec;
                    } else if (getNumericMonthDec >= 10) {
                        return getNumericMonthDec;
                    }
                }
            }

            var calendar_events = [
               @foreach($purchased_videos as $purchased_video)
                {
                    id: '<?= $purchased_video->sp_s_id; ?>',
                    title: '<?= $purchased_video->sp_s_title; ?>',
                    start: newDate.getFullYear() + '-'+ getDynamicMonth('default') +'-01T14:30:00',
                    end: newDate.getFullYear() + '-'+ getDynamicMonth('default') +'-02T14:30:00',
                    className: "bg-primary",
                    description: '<?= $purchased_video->sp_s_info; ?>'
                    },
                @endforeach
                @foreach($created_videos as $created_video)
                {
                    id: '<?= $created_video->sp_s_id; ?>',
                    title: '<?= $created_video->sp_s_title; ?>',
                    start: newDate.getFullYear() + '-'+ getDynamicMonth('default') +'-01T14:30:00',
                    end: newDate.getFullYear() + '-'+ getDynamicMonth('default') +'-02T14:30:00',
                    className: "bg-success",
                    description: '<?= $created_video->sp_s_info; ?>'
                },
                @endforeach
                ];


    </script>
    <script src="/plugins/fullcalendar/custom-fullcalendar.advance.js"></script>
    <!--  END CUSTOM SCRIPTS FILE  -->

@endsection
</body>
</html>