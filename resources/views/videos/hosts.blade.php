@extends('videos.master',['account' => $account])

@section('title')
    Account Details
@endsection

@section('css')
    <!-- BEGIN PAGE LEVEL CUSTOM STYLES -->
    <link href="/assets/css/scrollspyNav.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/components/cards/card.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/scrollspyNav.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/forms/theme-checkbox-radio.css">
    <!-- END PAGE LEVEL CUSTOM STYLES -->
@endsection

@section('content')

    <!--  BEGIN TOPBAR  -->

    @yield('fullnav', View::make('fullnav',['account' => $account]))

        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            <div class="layout-px-spacing">

                <div class="row layout-top-spacing" id="cancel-row">
                    <form class="row col-sm-12" id="search-form-hosts" onsubmit="return false;">
                        <div class="col-sm-8">
                            <div class="form-group">
                                <input type="text" class="form-control mb-4" name="search-field-hosts" id="search-field-hosts" placeholder="Search" value="">
                            </div>
                        </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <button type="button btn-success" class="form-control mb-4" name="search-hosts" id="search-hosts" value="Search">Search</button>
                        </div>
                    </div>
                    </form>

                    <div class="col-sm-12 clearfix">
                        <button id="filter-title" >Filters</button>
                    </div>

                    <div class="row filter-div col-sm-12 d-none">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="">Categories</label>
                                @foreach($categories as $category)
                                    <div class="col-sm-6">
                                        <div class="n-chk">
                                            <label class="new-control new-checkbox checkbox-primary">
                                                <input type="checkbox" name="categories[]" class="categories new-control-input filter-checkbox" value="{{ $category->sp_c_id }}">
                                                <span class="new-control-indicator"></span>{{ $category->sp_c_name }}
                                            </label>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <button type="button btn-primary" class="form-control mb-4" name="apply-filter" id="apply-filter"  value="Apply Filter">Apply Filter</button>
                            </div>
                        </div>
                    </div>
                    <div id="results" class="row col-sm-12">
                    <!--
                    <div id="app" style="width:100%">
                        <video-list csrf="{{ csrf_token() }}"></video-list>
                    </div>
                    -->
                        @foreach($hosts as $host)

                            <div class="card component-card_2" style="margin-top:20px" data-categories="{{ $host->sp_a_categories }}">
                                <img style="max-height:200px;max-width:100%" src="/storage/{{ $host->sp_a_photo }}" class="card-img-top" alt="widget-card-2">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $host->sp_a_business_name }}</h5>
                                    <p class="card-text">{{ $host->sp_a_description }}</p>
                                    @if($host->sp_a_business_url) <a href="#" class="btn btn-primary">{{ $host->sp_a_business_url }}</a> @endif
                                    <a href="/host/{{ md5($host->sp_a_id) }}" class="btn btn-success">View</a>
                                </div>
                            </div>

                        @endforeach

                    </div>
                </div>
        </div>
    <!-- END MAIN CONTAINER -->

@endsection

@section('code_footer')
        <script>
            var search_pg = 'searchHosts';
            var search_array_key = 'hosts';
            var count_total = <?php echo count($hosts); ?>;
        </script>
@endsection