@extends('master',['account' => $account])

@section('title')
    Account Details
@endsection

@section('css')
    <link href="/assets/css/scrollspyNav.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="/assets/css/forms/theme-checkbox-radio.css">
@endsection

@section('content')

    <!--  BEGIN TOPBAR  -->

    @yield('fullnav', View::make('fullnav',['account' => $account]))

    <!--  END TOPBAR  -->

    <div id="content" class="main-content">
        <div class="layout-px-spacing">

            <div class="account-settings-container layout-top-spacing">

                <div class="account-content">
                    <div class="scrollspy-example" data-spy="scroll" data-target="#account-settings-scroll" data-offset="-100">

                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
                                <form id="general-info" class="section general-info" method="post" action="upload" enctype="multipart/form-data">
                                    <div class="info">
                                        <h6 class="">Create Video</h6>
                                        <div class="row">
                                            <div class="col-lg-11 mx-auto">
                                                <div class="row">
                                                    <div class="col-xl-2 col-lg-12 col-md-4">
                                                        <div class="upload mt-4 pr-md-4">
                                                            <input type="file" name="video_image" id="input-file-max-fs" class="dropify" data-default-file="/assets/img/200x200.jpg" data-max-file-size="2M" />
                                                            <p class="mt-2"><i class="flaticon-cloud-upload mr-1"></i> Upload Image for Video</p>
                                                        </div>
                                                        <div id="upload_div" class="upload mt-4 pr-md-4 d-none">
                                                            <input type="file" name="video_file" id="input-file-max-fs" class="dropify" data-default-file="/assets/img/200x200.jpg" data-max-file-size="40M" />
                                                            <p class="mt-2"><i class="flaticon-cloud-upload mr-1"></i> Upload Video File</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-10 col-lg-12 col-md-8 mt-md-0 mt-4">
                                                        <div class="form">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="title">Title</label>
                                                                        <input type="text" class="form-control mb-4" name="title" id="title" placeholder="Video Title" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="info">Info</label>
                                                                        <textarea type="text" class="form-control mb-4" name="info" id="info" placeholder="Description" value=""></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="rate">Price</label>
                                                                        <input type="text" class="form-control mb-4" name="rate" id="rate" placeholder="0.00" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="type">Type</label>
                                                                        <select class="form-control" id="type" name="type">
                                                                            <option value="1">Stream</option>
                                                                            <option value="2">Upload</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="start_date">Start Date</label>
                                                                        <input type="text" class="form-control mb-4 date" name="start_date" autocomplete="false" id="start_date" placeholder="{{ date("m/d/Y H:00:00") }}" value="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="end_date">from Date</label>
                                                                        <input type="text" class="form-control mb-4 date" name="end_date" id="end_date" placeholder="{{ date("m/d/Y H:00:00") }}" value="">
                                                                    </div>
                                                                </div>



                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="access_length">Join After it Starts</label>
                                                                        <select class="form-control" id="join_start" name="join_start">
                                                                            <option value="0">No</option>
                                                                            <option value="1">Yes</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="expire">Expiration Date (optional)</label>
                                                                        <input type="text" class="form-control mb-4 date" name="expire" id="expire" placeholder="{{ date("m/d/Y H:00:00") }}" value="">
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="type">Video Active</label>
                                                                        <select class="form-control" id="active" name="active">
                                                                            <option value="1">Yes</option>
                                                                            <option value="0">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12" style="margin-bottom:10px">
                                                                    <h4>Recurring</h4>
                                                                </div>

                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="type">Recurring (same time and price)</label>
                                                                        <select class="form-control" id="recurring" name="recurring">
                                                                            <option value="0">No</option>
                                                                            <option value="1">Yes</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="type">Days to recur</label>
                                                                        <?php
                                                                        $timestamp = strtotime('next Sunday');
                                                                        $days = array();
                                                                        for ($i = 0; $i < 7; $i++) {
                                                                            $days[] = strftime('%A', $timestamp);
                                                                            $timestamp = strtotime('+1 day', $timestamp);
                                                                        }
                                                                        ?>
                                                                        @foreach($days as $dk => $date)
                                                                            <div class="n-chk">
                                                                                <label class="new-control new-checkbox checkbox-primary">
                                                                                    <input type="checkbox" name="recurring_days[]" class="new-control-input" value="{{ $dk }}">
                                                                                    <span class="new-control-indicator"></span>{{ $date }}
                                                                                </label>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12" style="margin-bottom:10px">
                                                                <h4>Categories</h4>
                                                                </div>

                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label for="phone">Categories</label>
                                                                        @foreach($categories as $category)
                                                                            <div class="n-chk">
                                                                                <label class="new-control new-checkbox checkbox-primary">
                                                                                    <input type="checkbox" name="categories[]" class="new-control-input" value="{{ $category->sp_c_id }}">
                                                                                    <span class="new-control-indicator"></span>{{ $category->sp_c_name }}
                                                                                </label>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="account-settings-footer">

                    <div class="as-footer-container">

                        <button id="multiple-reset" class="btn btn-warning">Reset All</button>
                        <div class="blockui-growl-message">
                            <i class="flaticon-double-check"></i>&nbsp; Settings Saved Successfully
                        </div>
                        <button id="multiple-messages" class="btn btn-primary">Save Changes</button>

                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection

@section('code_footer')
    <script>
        $(function(){
           $('#type').change(function(){
              if($(this).val() == 1)
              {
                  $('#upload_div').addClass("d-none");
              }
              else
              {
                  $('#upload_div').removeClass("d-none");
              }
           });

            var date = new Date();
            var y = date.getFullYear();
            var d = date.getDate();
            var m = date.getMonth();

            $( ".date" ).datetimepicker(
                {
                    format: "m/d/Y H:i:s",
                    minDate: (m+1)+'/'+d+'/'+y,
                    allowTimes:[
                        @for($x=0; $x<24; $x++)

                        <?= "'".$x.":00','". $x.":30',"; ?>

                        @endfor
                    ]
                }
            );
        });
    </script>
@endsection