
var host = 'http://local.spotlight.com';

var ua = navigator.userAgent,
    touchClick = (ua.match(/iPad/i)) ? "touchstart" : "click";

var device = (ua.match(/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini|mobile|tablet/i)) ? "mobile" : "desktop";

var baseURL = host+'/';
var loadIMG_url = host+'/assets/img/loader.gif';
var loadIMG = '<div id="loadIMGdiv" style="">'+
    '<img src="'+ loadIMG_url +'" alt="ajax load" style="margin:auto" />'+
    '</div>';
var baseHOST = host + '/';
var pg_count = 1;
var post_lock = false;

function ValidateEmail(mail)
{
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if(mail.match(mailformat))
    {
        return true;
    }
    return false;
}


$(function(){

    $('.check-input').keyup(function()
    {
        var val = $(this).val();
        var f = $(this);
        var field = $(this).attr('data-field');
        if(ac_username == val || ac_email == val)
        {
            $('#'+field+'-alert').text( '' ).removeClass('text-danger,text-success');
            $('#'+field+'-alert').text('');
            f.removeClass('red-border');
            return true;
        }

        post_lock = true;

        if((ValidateEmail(val) && field == 'email') || (field == 'username' && val.length >= 8))
        {
            $('#'+field+'-alert').text('');
            $.post( baseURL + 'api/register/checkField',{field:field,value:val},function(data)
            {
                console.log(data.data);
                if(data.errors)
                {
                    $('#'+field+'-alert').text( data.errors.email ).addClass('text-danger').removeClass('text-success');
                }
                else if(data.count == 0)
                {
                    $('#'+field+'-alert').text( 'Successful' ).removeClass('text-danger').addClass('text-success');;
                    post_lock = false;
                }
                else if(data.count > 0)
                {
                    $('#'+field+'-alert').text( field.toUpperCase() + ' already exists!' ).addClass('text-danger').removeClass('text-success');
                }
            });
            f.removeClass('red-border');
        }
        else
        {
            $('#'+field+'-alert').html('');
            f.addClass('red-border');
        }
    });

    $(window).on("beforeunload", function()
    {
        alert(1);
        $.get(baseHOST + 'logout');
    });

    $('#apply-filter').click(function()
    {
        var filters = [];
        $.each($('.categories'),function(x,y)
        {
            if($(y).prop('checked'))
            {
                filters[filters.length] = y.value;
            }
        });

        $('.card').addClass('d-none');
        if(filters.length > 0)
        {
            $.each($('.card'),function(x,y)
            {
                var card = y;
                var categories = JSON.parse( $(y).attr('data-categories') );
                console.log(categories);
                $.each(categories,function(x,y)
                {
                    if($.inArray(y,filters) > -1)
                    {
                        $(card).removeClass('d-none');
                    }
                });

            });
        }
        else
        {
            $('.card').removeClass('d-none');
        }
        $('.filter-div').addClass('d-none');
    });

    $('#filter-title').click(function()
    {
        $('.filter-div').toggleClass('d-none');
    });

    $('#search-form-video').submit(function()
    {
        searchVideo();
    });

    $('#search-video').click(function(e)
    {
        e.preventDefault(e);
        searchVideo();
    });

    $('#save-filter-video').click(function(e)
    {
        e.preventDefault(e);
        $('.filter-div').addClass('d-none');
        searchVideo();
    });

    function searchVideo()
    {
        search_pg = 'searchVideo';
        pg_count = 1;
        var s = $('#search-field-video').val();
        var f = [];
        $.each($('.filter-checkbox'),function(x,y){
            if($(y).prop('checked'))
            {
                f[x] = y.value;
            }
        });

        var post_data = {
            search:s,
            filters:f,
            page:1
        };

        $.post( baseURL + 'api/video/' + search_pg,post_data,function(data)
        {
            var html = '';
            for(var x in data['videos'])
            {
                html += '<div class="card component-card_2" style="margin-top:20px">' +
                    '                                <img style="max-height:200px;max-width:100%" src="/storage/'+ data['videos'][x].sp_s_image_file +'" class="card-img-top" alt="widget-card-2">\n' +
                    '                                <div class="card-body" data-categories="'+ data['videos'][x].sp_s_categories +'">' +
                    '                                    <h5 class="card-title">'+ data['videos'][x].sp_s_title +'</h5>' +
                    '                                    <p class="card-text">'+ data['videos'][x].sp_s_info +'</p>' +
                    '                                    <a href="#" class="btn btn-primary">'+ data['videos'][x].sp_s_rate +' '+ data['videos'][x].sp_a_currency +'</a>' +
                    '                                        <br><br><a href="/video/edit/?v_id='+ data['videos'][x].sp_s_stream_id +'" class="btn btn-warning">edit</a>' +
                    '                                </div>' +
                    '                            </div>';
            }
            $('#results').html( html );

        });
    }

    $('#search-form-hosts').submit(function()
    {
        searchHosts();
    });

    $('#search-hosts').click(function(e)
    {
        e.preventDefault(e);
        searchHosts();
    });

    $('#save-filter-hosts').click(function(e)
    {
        e.preventDefault(e);
        $('.filter-div').addClass('d-none');
        searchHosts();
    });

    function searchHosts()
    {
        search_pg = 'searchHosts';
        pg_count= 1;
        var s = $('#search-field-hosts').val();
        var f = [];
        $.each($('.filter-checkbox'),function(x,y){
            if($(y).prop('checked'))
            {
                f[x] = y.value;
            }
        });

        var post_data = {
            search:s,
            filters:f,
            page:1
        };

        $.post( baseURL + 'api/video/' + search_pg,post_data,function(data)
        {
            var html = '';
            for(var x in data['hosts'])
            {
                html += '<div class="col-lg-6 mx-auto" style="float: left;">' +
                    '<div class="card component-card_2" style="margin-top:20px">' +
                    '                                <img style="max-height:200px;max-width:100%" src="/storage/'+ data['hosts'][x].sp_a_photo +'" class="card-img-top" alt="widget-card-2">\n' +
                    '                                <div class="card-body" data-categories="'+ data['hosts'][x].sp_a_categories +'">' +
                    '                                    <h5 class="card-title">'+ data['hosts'][x].sp_a_business_name +'</h5>' +
                    '                                    <p class="card-text">'+ data['hosts'][x].sp_a_description +'</p>' +
                    '                                    <a href="#" class="btn btn-primary">'+ data['hosts'][x].sp_a_business_url +'</a>' +
                    '                                    <a href="/host/\'+ data[x].md5_sp_a_id +\'" class="btn btn-success">View Host</a>' +
                    '                                </div>' +
                    '                                </div>' +
                    '                            </div>';
            }
            $('#results').html( html );

        });
    }

    $( window ).scroll(function()
    {
        var scrollHeight = $(document).height();
        var scrollPosition = $(window).height() + $(window).scrollTop();
        if ((scrollHeight - scrollPosition) / scrollHeight < 0.1
            &&
            count_total > 0
            &&
            post_lock == false)
        {
            ++pg_count; console.log(pg_count);
            var s = $('#search-field-hosts').val();
            var f = [];
            $.each($('.filter-checkbox'),function(x,y)
            {
                if($(y).prop('checked'))
                {
                    f[x] = y.value;
                }
            });
            var post_data = {
                search:s,
                filters:f,
                page:pg_count
            };
            post_lock = true;

            $.post( baseURL + 'api/video/' + search_pg,post_data,function(data)
            {
                count_total = data[ search_array_key ].length;
                var html = '';
                for(var x in data[ search_array_key ])
                {

                    if(search_array_key == 'videos')
                    {
                        html += '<div class="col-lg-6 mx-auto" style="float: left;">' +
                            '<div class="card component-card_2" style="margin-top:20px">' +
                            '                                <img style="max-height:200px;max-width:100%" src="/storage/'+ data['videos'][x].sp_s_image_file +'" class="card-img-top" alt="widget-card-2">\n' +
                            '                                <div class="card-body" data-categories="'+ data['videos'][x].sp_s_categories +'">' +
                            '                                    <h5 class="card-title">'+ data['videos'][x].sp_s_title +'</h5>' +
                            '                                    <p class="card-text">'+ data['videos'][x].sp_s_info +'</p>' +
                            '                                    <a href="#" class="btn btn-primary">'+ data['videos'][x].sp_s_rate +' '+ data['videos'][x].sp_a_currency +'</a>' +
                            '                                        <br><br><a href="/video/edit/?v_id='+ data['videos'][x].sp_s_stream_id +'" class="btn btn-warning">edit</a>' +
                            '                                </div>' +
                            '                                </div>' +
                            '                            </div>';
                    }
                    else if( search_array_key == 'hosts' )
                    {
                        var business_url = '';
                        if(data['hosts'][x].sp_a_business_url)
                        {
                            business_url = '<a href="#" class="btn btn-primary">'+ data['hosts'][x].sp_a_business_url +'</a>';
                        }

                        html += '<div class="col-lg-6 mx-auto" style="float: left;">' +
                            '<div class="card component-card_2" style="margin-top:20px;">' +
                            '                                <img style="max-height:200px;max-width:100%" src="/storage/'+ data['hosts'][x].sp_a_photo +'" class="card-img-top" alt="widget-card-2">\n' +
                            '                                <div class="card-body" data-categories="'+ data['hosts'][x].sp_a_categories +'">' +
                            '                                    <h5 class="card-title">'+ data['hosts'][x].sp_a_business_name +'</h5>' +
                            '                                    <p class="card-text">'+ data['hosts'][x].sp_a_description +'</p>' + business_url +
                            '                                    <a href="/host/'+ data[x].md5_sp_a_id +'" class="btn btn-success">View Host</a>' +
                            '                                </div>' +
                            '                                </div>' +
                            '                            </div>';
                    }
                }
                $('#results').append( html ).css({'margin-bottom':'200px'});
                post_lock = false;
            });
        }
    });

    $('body').on("click",".set-default",function()
    {
        var type = $(this).attr('data-type');
        var id = $(this).attr('data-id');

        if(post_lock == false)
        {

            $.post( baseURL + 'api/'+ type +'/default/' + id,{},function(data)
            {
                if(data[ type ] == false)
                {
                    $('#add_card_form_errors').html('Error 207');
                    return false;
                }
                post_lock = true;
                var html = '';
                for(var x in data[ type ])
                {

                    if(type == 'banks')
                    {

                        if( data[ type ][x].sp_b_default == 0 )
                        {
                            var def = '<button class="btn btn-primary set-default" data-type="banks" data-id="'+ data[ type ][x].sp_b_stripe_id.substr(3) +'" data-id="'+ data[ type ][x].sp_b_name +'" onclick="return false">Set as Default</button>';
                        }
                        else
                        {
                            var def = '<br><b>Default</b>';
                        }

                        html += '<div class="col-lg-6 mx-auto" style="float: left;">' +
                            '                                                                    <div class="card component-card_2" style="margin-top:20px;text-align: center;padding-top:10px"' +
                            '                                                                     data-categories="">' +
                            '                                                                       <div class="card-body" style="">' +
                            '                                                                           <div class="col-lg-6 pull-left" style="float: left;">' +
                            '                                                                                <h5 class="card-title">'+ data[ type ][x].sp_b_name +'</h5>' +
                            '                                                                                <p class="card-text">'+ data[ type ][x].sp_b_bank +'</p>' +
                            '                                                                                <p class="card-text">'+ data[ type ][x].sp_b_account_number_4 +'</p>' +
                            '                                                                                <p class="card-text">'+ data[ type ][x].sp_b_currency +'</p>' +
                            '                                                                           </div>' +
                            '                                                                           <div class="col-lg-6 pull-left" style="float: left;">' +
                            '                                                                               <a href="'+ baseURL +'banks/view/'+ data[ type ][x].sp_b_stripe_id.substr(3) +'">' +
                            '                                                                                    <button class="btn btn-primary edit-bank" data-id="'+ data[ type ][x].sp_b_name +'" onclick="return false">Edit</button>' +
                            '                                                                                </a>' +
                            '                                                                                <button class="btn btn-primary set-remove" data-type="banks" data-id="'+ data[ type ][x].sp_b_stripe_id.substr(3) +'" onclick="return false">Remove</button><br><br>'+
                                                                                                        def+
                            '                                                                           </div>' +
                            '                                                                        </div>' +
                            '                                                                </div>' +
                            '                                                            </div>'
                    }
                    else if( type == 'cards' )
                    {

                        if( data[ type ][x].sp_ca_default == 0 ){
                            var def = '<button class="btn btn-primary set-default" data-type="cards" data-id="'+ data[ type ][x].sp_ca_stripe_id.substr(3) +'" data-id="'+ data[ type ][x].sp_ca_name +'" onclick="return false">Set as Default</button>';
                        }else{
                            var def = '<br><b>Default</b>';
                        }

                        html += '<div class="col-lg-6 mx-auto" style="float: left;">' +
                            '<div class="card component-card_2" style="margin-top:20px" data-categories="">' +
                            '                                                                <div class="card-body">' +
                            '                                                                    <h5 class="card-title">'+ data[ type ][x].sp_ca_name +'</h5>' +
                            '                                                                    <p class="card-text">'+ data[ type ][x].sp_ca_type +'</p>' +
                            '                                                                    <p class="card-text">'+ data[ type ][x].sp_ca_last_4 +'</p>' +
                            '                                                                    <button class="btn btn-primary set-remove" ddata-id="'+ data[ type ][x].sp_ca_stripe_id.substr(3) +'" onclick="return false">Remove</button><br><br>'+
                                                                                                def+
                            '                                                                </div>' +
                            '                                                              </div>' +
                            '                                                            </div>';
                    }
                }
                $('#' + type).html( html );
                post_lock = false;
            });
        }

    });

    $('body').on("click",".set-remove",function()
    {
        var type = $(this).attr('data-type');
        var id = $(this).attr('data-id');

        if(post_lock == false)
        {

            $.post( baseURL + 'api/'+ type +'/default/' + id,{},function(data)
            {
                if(data[ type ] == false)
                {
                    $('#add_card_form_errors').html('Error 205');
                    return false;
                }
                post_lock = true;
                var html = '';
                for(var x in data[ type ])
                {

                    if(type == 'banks')
                    {

                        if( data[ type ][x].sp_b_default == 0 )
                        {
                            var def = '<button class="btn btn-primary set-default" data-type="banks" data-id="'+ data[ type ][x].sp_b_stripe_id +'" data-id="'+ data[ type ][x].sp_b_name +'" onclick="return false">Set as Default</button>';
                        }
                        else
                        {
                            var def = '<br><b>Default</b>';
                        }

                        html += '<div class="col-lg-6 mx-auto" style="float: left;">' +
                            '                                                                    <div class="card component-card_2" style="margin-top:20px;text-align: center;padding-top:10px"' +
                            '                                                                     data-categories="">' +
                            '                                                                       <div class="card-body" style="">' +
                            '                                                                           <div class="col-lg-6 pull-left" style="float: left;">' +
                            '                                                                                <h5 class="card-title">'+ data[ type ][x].sp_b_name +'</h5>' +
                            '                                                                                <p class="card-text">'+ data[ type ][x].sp_b_bank +'</p>' +
                            '                                                                                <p class="card-text">'+ data[ type ][x].sp_b_account_number_4 +'</p>' +
                            '                                                                                <p class="card-text">'+ data[ type ][x].sp_b_currency +'</p>' +
                            '                                                                           </div>' +
                            '                                                                           <div class="col-lg-6 pull-left" style="float: left;">' +
                            '                                                                               <a href="'+ baseURL +'banks/view/'+ data[ type ][x].sp_b_stripe_id +'">' +
                            '                                                                                    <button class="btn btn-primary edit-bank" data-id="'+ data[ type ][x].sp_b_name +'" onclick="return false">Edit</button>' +
                            '                                                                                </a>' +
                                                                                                            def+
                            '                                                                           </div>' +
                            '                                                                        </div>' +
                            '                                                                </div>' +
                            '                                                            </div>'
                    }
                    else if( type == 'cards' )
                    {

                        if( data[ type ][x].sp_ca_default == 0 ){
                            var def = '<button class="btn btn-primary set-default" data-type="cards" data-id="'+ data[ type ][x].sp_ca_stripe_id +'" data-id="'+ data[ type ][x].sp_ca_name +'" onclick="return false">Set as Default</button>';
                        }else{
                            var def = '<br><b>Default</b>';
                        }

                        html += '<div class="card component-card_2" style="margin-top:20px" data-categories="">' +
                            '                                                                <div class="card-body">' +
                            '                                                                    <h5 class="card-title">'+ data[ type ][x].sp_ca_name +' </h5>' +
                            '                                                                    <p class="card-text"><b>'+ data[ type ][x].sp_ca_type +'</b> '+ data[ type ][x].sp_ca_last_4 +'</p>' +
                            '                                                                    <p class="card-text">'+ data[ type ][x].sp_ca_month +' / '+data[ type ][x].sp_ca_year +'</p>' +
                                                                                                def+
                            '                                                                </div>' +
                            '                                                            </div>';
                    }
                }
                $('#' + type).html( html );
                post_lock = false;
            });
        }

    });

    $('.choose-country').change(function()
    {
        var id = $('option:selected', this).attr('data-code');
        $.get( baseURL + 'api/loc/states',{country_id:id},function(data)
        {
            var html = '<option value="">Select State/Province</option>';
            for(var x in data.states)
            {
                html += '<option data-code="'+ data.states[x].id +'" value="'+ data.states[x].iso2 +'">'+ data.states[x].name +'</option>';
            }
            console.log(html);
            $('#state').html(html);
        });
    });

    $('.choose-state').change(function()
    {
        var country_id = $('option:selected', $('#country')).attr('data-code');
        var id = $('option:selected', this).attr('data-code');
        $.get( baseURL + 'api/loc/cities',{country_id:country_id,state_id:id},function(data)
        {
            var html = '<option value="">City</option>';
            for(var x in data.cities)
            {
                html += '<option data-code="'+ data.cities[x].id +'" value="'+ data.cities[x].iso2 +'">'+ data.cities[x].name +'</option>';
            }
            $('#city').html(html);
        });
    });

});


