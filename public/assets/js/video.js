var videoClient;
var activeRoom;
var previewMedia;
var identity;
var roomName;

var host = 'https://spotlight.varspec.com';
var baseHOST = host + '/';

// Check for WebRTC
if (!navigator.webkitGetUserMedia && !navigator.mozGetUserMedia) {
  alert('WebRTC is not available in your browser.');
}


// When we are about to transition away from this page, disconnect
// from the room, if joined.
window.addEventListener('beforeunload', leaveRoomIfJoined);
var v_id = $('#room-id').val();
$.getJSON(baseHOST+'video/token', {'v_id':v_id}, function (data) {
  var identity = data.identity;
  var token = data.token;
  // Create a Video Client and connect to Twilio

  // Bind button to join room
  document.getElementById('button-join').onclick = function () {
    var me = document.getElementById('user').value;
    roomName = document.getElementById('room-name').value;
    
    
    $.getJSON(baseHOST+'video/start', function (data) {
    if (data.success == 1) {
      if (roomName)
      {
          Twilio.Video.createLocalTracks({
              audio: true,
              video: { width: 300 }
          }).then(function(localTracks) {
              return Twilio.Video.connect(token, {
                  name: roomName,
                  tracks: localTracks,
                  video: { width: 300 }
              });
          }).then(function(room) {
              console.log('Successfully joined a Room: ', room.name);

              room.participants.forEach(participantConnected);

              var previewContainer = document.getElementById(room.localParticipant.sid);
              if (!previewContainer || !previewContainer.querySelector('video')) {
                  participantConnected(room.localParticipant);
              }

              room.on('participantConnected', function(participant) {
                  console.log("Joining: '"+ participant.identity  + "'");
                  participantConnected(participant);
              });

              room.on('participantDisconnected', function(participant) {
                  console.log("Disconnected: '"+  participant.identity + "'");
                  participantDisconnected(participant);
              });
          });
          // additional functions will be added after this point

      } else {
        alert('Please enter a room name.');
      }
    }else{
      alert(data.error);
      alert('not logged');
    }
      
    
    })
  };

  // Bind button to leave room
  document.getElementById('button-leave').onclick = function () {
    log('Leaving room...');
    activeRoom.disconnect();
      me = document.getElementById('user').value;
      e = document.getElementById(me);
      e.classList.remove("joined");
  };
});


function participantConnected(participant) {
    console.log('Participant "%s" connected', participant.identity);

    const div = document.createElement('div');
    div.id = participant.sid;
    div.setAttribute("style", "float: left; margin: 10px;");
    div.innerHTML = "<div style='clear:both'>" + participant.identity + "</div>";

    participant.tracks.forEach(function(track) {
        trackAdded(div, track)
    });

    participant.on('trackAdded', function(track) {
        trackAdded(div, track)
    });
    participant.on('trackRemoved', trackRemoved);

    document.getElementById('local-media').appendChild(div);
}

function participantDisconnected(participant) {
    console.log('Participant "%s" disconnected', participant.identity);

    participant.tracks.forEach(trackRemoved);
    document.getElementById(participant.sid).remove();
}

function trackAdded(div, track) {
    div.appendChild(track.attach());
    var video = div.getElementsByTagName("video")[0];
    if (video) {
        video.setAttribute("style", "max-width:300px;");
    }
}

function trackRemoved(track) {
    track.detach().forEach( function(element) { element.remove() });
}

// Successfully connected!
function roomJoined(room) {
  activeRoom = room;
    console.log(room.participants);
  
  room.participants.forEach(function(participant) {
    participant.media.detach();
   e = document.getElementById(participant.identity);
      e.className += " joined";
  });

  log("Joined as '" + identity + "'");
  document.getElementById('button-join').style.display = 'none';
  document.getElementById('button-leave').style.display = 'inline';

  // Draw local video, if not already previewing
  if (!previewMedia) {
    room.localParticipant.media.attach('#local-media');
  }

  room.participants.forEach(function(participant) {
    log("Already in Room: '" + participant.identity + "'");
    participant.media.attach('#remote-media');
  });

  // When a participant joins, draw their video on screen
  room.on('participantConnected', function (participant) {
    log("Joining: '" + participant.identity + "'");
    e = document.getElementById(participant.identity);
      e.className += " joined";

    participant.media.attach('#remote-media');
  });

  // When a participant disconnects, note in log
  room.on('participantDisconnected', function (participant) {
    log("Participant '" + participant.identity + "' left the room");
    e = document.getElementById(participant.identity);
      e.classList.remove("joined");
    participant.media.detach();
  });

  // When we are disconnected, stop capturing local video
  // Also remove media for all remote participants
  room.on('disconnected', function () {
    $.getJSON(baseHOST+'video/en_log', function (data) {
      
    });
    log('Left');
    room.localParticipant.media.detach();
    room.participants.forEach(function(participant) {
      participant.media.detach();
      e = document.getElementById(participant.identity);
      e.classList.remove("joined");
    });
    activeRoom = null;
    document.getElementById('button-join').style.display = 'inline';
    document.getElementById('button-leave').style.display = 'none';
  });
}

//  Local video preview
document.getElementById('button-preview').onclick = function () {
  if (!previewMedia) {
    previewMedia = new Twilio.Video.LocalMedia();
    Twilio.Video.getUserMedia().then(
    function (mediaStream) {
      previewMedia.addStream(mediaStream);
      previewMedia.attach('#local-media');
    },
    function (error) {
      console.error('Unable to access local media', error);
      log('Unable to access Camera and Microphone');
    });
  };
};

// Activity log
function log(message) {
  var logDiv = document.getElementById('log');
  logDiv.innerHTML += '<p>&gt;&nbsp;' + message + '</p>';
  logDiv.scrollTop = logDiv.scrollHeight;
}

function leaveRoomIfJoined() {
  if (activeRoom) {
    activeRoom.disconnect();
    $.getJSON(baseHOST+'video/en_log', function (data) {
      
    });
  }
}